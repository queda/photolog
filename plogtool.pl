#! /usr/bin/perl

use Getopt::Std;
use DBI;

$exif_tool = 'jhead';

sub book_find()
{
	my $path = $_[0];
	my $p_id = $_[1];

	$sql = "SELECT id,p_id,c_num FROM photolog.book WHERE name=?";
	$sth = $dbh->prepare($sql);
	$sth->execute( $path );
	
	@row = $sth->fetchrow_array;
	$p_id = $row[1];

#	print "bf: ($path) $row[0] $row[1] \n";
	return $row[0];
}

sub book_create()
{
	my $path = $_[0];
	my $bid = 0;
	my $pid = 0;

#	print "bc: $path \n";

	if ( !($bid = &book_find($path,\$pid)) ) {

		$ri = rindex($path,'/');
		if ( $ri >= 0 && $pid == 0 ){
			$ppath = substr($path,0,$ri);
			$pid = &book_create( $ppath );
		}
		
		$sql = "INSERT INTO photolog.book SET "
			  ."p_id=?,name=?,status='d',datedesc=?,user_id=?"; 
		$sth = $dbh->prepare($sql);
		$sth->execute( $pid,$path,$path,1 );

		$bid = &book_find($path,\$pid);
		
		if ( $pid != 0) {
			$sql = "UPDATE photolog.book SET c_num=c_num+1 WHERE id=?";
			$sth = $dbh->prepare($sql);
			$sth->execute( $pid );
		}
	}
	return $bid;
}

sub Exif_data()
{
	my $file = $_[0];
	my $size = $_[1];
	my $mod_time = $_[2];
	my $img_res = $_[3];
	
	open EXIF, "$exif_tool $file|";
	@exif = <EXIF>;
	close EXIF;

	for ($i=0; $i< $#exif; $i++) {
		@fields = split(/ *: /, $exif[$i],2);
		chomp($fields[1]);

#		print "k: ($fields[0]) v: ($fields[1])\n";
		$exif_data{$fields[0]} = $fields[1];
	}		

	$$size = $exif_data{'File Size'};
	if ( $exif_data{'Date/Time'} ne '' ) {
		$$mod_time = $exif_data{'Date/Time'};
	}
	else {
		$$mod_time = $exif_data{'File date'};
	}
	$$img_res = $exif_data{'Resolution'};
		
#	$$size = int substr($exif[1],index($exif[1],':')+2);
	# some exif data
#	if ($#exif>=6) {
#		$$mod_time = substr($exif[5],index($exif[5],':')+2);
#		$$img_res = substr($exif[6],index($exif[6],':')+2);
#	}
#	else {
#		$$mod_time = substr($exif[2],index($exif[2],':')+2);
#		$$img_res = substr($exif[3],index($exif[3],':')+2);
#	}
#	chomp $$img_res;

	# drop 1st three lines
	shift(@exif); shift(@exif); shift(@exif);

	return @exif;
}

sub Load_images()
{
	my ($cwd, $i, $m) = @_;
	my @imgs = @$i;
	my @movs = @$m;
	my $mod_time;
	my $img_file;
	my $size;
	my $dir;
	my $parent;
	
	$dir = $args{d}.( $cwd ne "." ? '/' : '').substr($cwd,2);

	# create a book entry if needed
#	$x = index($dir,"/",1);
#	if ( $x>=0 ){ &book_create(substr($dir,1,$x-1)); }
	
#	mkdir "$cwd/.s", 0777;

	$sql = "INSERT INTO photolog.image SET name=?,dir=?,url=?,media=?"
			.",cd=?,size=?,res=?,exif=?,user_id=?,status='a'";
	$sth = $dbh->prepare($sql);
	
	while (($_ = pop @imgs))
	{
		$img_file = "$cwd/$_";

		@exif = &Exif_data($img_file,\$size,\$mod_time,\$img_res);

		$sth->execute( $_,$dir,'','i',$mod_time,$size,$img_res,join('',@exif),1 );
	}
	while (($_ = pop @movs))
	{
		$img_file = "$cwd/$_";
		$thm_file = $img_file;
		$thm_file =~ s/.avi/.thm/i;
		
		$size = 0;
		$mod_time = 0;
		$img_res = 0;

		if (-e $thm_file) {
			@exif = &Exif_data($thm_file,\$size,\$mod_time,\$img_res);
		}
		$sth->execute( $_,$dir,'','v',$mod_time,$size,$img_res,join('',@exif),1 );
	}
}

sub Get_dirlist()
{
	my $path = $_[0];
	my @dirlist;

	opendir DIR, $path;
	while ( ($_ = readdir DIR) )
	{
		# strip all dot (.) files from dir list
		if ( ! /^\..*/ )
		{
			unshift @dirlist, $_;
		}
	}
	closedir DIR;

	return sort @dirlist;
}

sub Recurse_dirs()
{
	my $dir = $_[0];
	my @dirlist;
	my $ident = $dir;
	my @dirs;
	my @imgs;
	my @movs;
	my %parms;

	print "$dir: ";
	
	$ident =~ s/./ /g;

	@dirlist = &Get_dirlist( $dir );
	while ( ($_ = shift( @dirlist )) )
	{
		# found a dir, lets recurse!
		if ( -d "$dir/$_" )
	   	{
			push @dirs, "$dir/$_";
	   	}
		elsif ( /.*\.(jpg)$/i ) {
			unshift @imgs, $_;
		}
		elsif ( /.*\.(avi|mov)$/i ) {
			unshift @movs, $_;
		}
	}
	@dirlist = @dirs;
	
	print " \td(". ($#dirs +1) .") " if @dirs;

	print " \ti(". ($#imgs + 1) .") " if @imgs;
	print "m(". ($#movs + 1) .") " if @movs;

	# load images into database
	&Load_images( $dir, \@imgs, \@movs ) if $args{l} && (@imgs || @movs);

	# print out our index if needed
#	&make_indexs( $dir, \@dirs, \@imgs, \@movs ) if ($args{i} || $args{h}) && (@imgs || @dirs || @movs);
	
	print "\n";
	# lastly, recurse any found subdirs
	while ( $args{r} && ($_ = shift( @dirlist )) )
	{
		&Recurse_dirs( $_ );
	}
}

sub Usage()
{
	print "Usage $0 -rl d<pfx=/2002/11> p<prefix>\n";
	print "\n";
	exit;
}

&getopts( 'rld:p:?', \%args );

&Usage() if (!%args || $args{'?'} );

$dbh = DBI->connect( "DBI:mysql:photolog:localhost", "paulmcav","lepton96")
	or die $DBI::errstr;

print "Scanning directories:\n";

&Recurse_dirs( '.' );

print "\n";
exit;

