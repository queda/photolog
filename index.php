<?php # $Id: index.php,v 1.5 2003-07-10 21:02:02 paulmcav Exp $

	include('./header.inc.php');

	# found a userid cookie, go get name if not set
	if ($HTTP_COOKIE_VARS['userid'] >0 && $session['userem'] == '')
	{
		ValidateUser( $HTTP_COOKIE_VARS['userid'], -1 );
	}
	// determine which site user is coming from/to
	if ($session['srv_nm'] != $_SERVER['SERVER_NAME']) {
		LocateSite( $_SERVER['SERVER_NAME'] );
	}

	if ( ! $_REQUEST['page'] )
	{
		$_REQUEST['page'] = 'Index';
	}
	$obj = CreateObject( $_REQUEST['page'] );

#	echo "<pre>"; print_r($GLOBALS['COOKIES']); echo "</pre>";

	$obj->display();
