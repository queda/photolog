<?php // $Id: media.php,v 1.30 2004-04-17 03:25:07 paulmcav Exp $

// ARGS:

// g    generate images only, don't passthru
// dbid	database image id
// s    scale of image to display


include( './header.inc.php' );

/* BASE64 QUERY_STRING decoding */
if ( ($qrystr = $_SERVER['QUERY_STRING']) != '' ) {
	$de64 = base64_decode( $qrystr );
	if ( $de64!='' ) { parse_str($de64,$_GET); } else { $de64 = $qrystr; }
	$_REQUEST = array_merge($_GET,$_REQUEST);
}

/* media constants */
$_Image_Dir = "/data/photos";
$_Image_Dat = "dat";
$_Image_Rez = array(0,1024,800,534,400,300,'t','i'=>85,'p'=>85,'v'=>160);
#$_jpeg_cmpv = array(0,55  ,60 ,65 , 75 );
$_Mime_List = array( '' => 'application/octet-stream',
					'avi' => 'video/x-msvideo',
					'mpg' => 'video/mpeg',
					'mov' => 'video/quicktime',
					);

//$_Image_Dir = $config['img_dir'];
//$_Image_Dat = $config['dat_dir'];
//$_Image_Rez = cfg_to_array( $config, 'size' );

$db = $globs['db'];

/* gather args  */
$_G    = $_REQUEST['g'];		// generate image only
$_DBID = $_REQUEST['img'];		// database image id (dbid)
$_S    = $_REQUEST['s'];		// image size selection
$_F    = $_REQUEST['f'];		// imaga filename

// --- START: main() ---

#echo "_G: $_G<br>";
#echo "_DBID: $_DBID<br>" ;
#echo "_S: $_S<br>" ;
#echo "_F: $_F<br>";

/* db image id was passed */
if ( $_DBID!='' ) {
	$row = db_media_get( $db, $_DBID );

	$_IMG_UID = $row['user_id'];
	$_IMG_P   = $row['dir'];
	$_IMG_N   = $row['name'];
	$_IMG_T   = $row['media'];

	$_IMG_PATH = $_IMG_UID.$_IMG_P;
	#echo "<pre>"; print_r( $row ); echo "</pre>";

	$_IMG_TXT = chr(169).' '.$row['year'].' '.$row['email'];
}
/* breakout image name parts */
else {
	// strip of png if file is supposed to be a thumbnail
	$_IMG_T = 'i';
	if ( (substr( $_F, -4 ) == ".png" )) {
		$_F = substr( $_F,0,-4 );
		$_S = "t";

		if ( (substr( $_F, 0,4 ) == "dat/") ) {
			$_F = substr( $_F, 4 );
		}
	}
	if ( (substr( $_F, -4 ) == ".thm" )) {
		#		$_F = substr( $_F,0,-4 ).".avi";
		$_S = "t";

		if ( (substr( $_F, 0,4 ) == "dat/") ) {
			$_F = substr( $_F, 4 );
		}
		$_IMG_T = 'v';
	}

	$_IMG_P = dirname( $_F );
	$_IMG_N = basename( $_F );

	$_IMG_PATH = $_IMG_P;
}

/* check to show a thumbnail of the image */
if ( $_S == 't' ) { $_thumb = 1; } else { $_thumb = 0; }

if ( $_S >= sizeof($_Image_Rez)-2 || $_S=='' ) $_S=sizeof($_Image_Rez)-3;
$img_xy = $_Image_Rez[$_S];

$_IMG_X = substr($_IMG_N,-3);
$_IMG_N = substr($_IMG_N,0,-3);

$_SRC_IMG = $_Image_Dir.'/'.$_IMG_PATH.'/'.$_IMG_N.$_IMG_X;
$_DST_PTH = $_Image_Dir."/$_Image_Dat/".$_IMG_PATH;
	
/* if displaying a thumbnail or not, vary depending on media type */
if ( $_thumb ) {
	if ( $_IMG_T == 'v' ) {		/* video */
		$_DST_IMG = $_Image_Dir.'/'.$_IMG_PATH.'/'.$_IMG_N."thm";
		if ( !file_exists( $_DST_IMG )) {
			$_SRC_IMG = $_Image_Dir.'/'.$_IMG_PATH.'/'.$_IMG_N."THM";
			$_DST_IMG = $_SRC_IMG;
		}
	} else {
		$_DST_IMG = $_DST_PTH."/$_IMG_N$_IMG_X.png";
	}
	$img_xy = $_Image_Rez[$_IMG_T];
}
else {
	if ( $_IMG_T == 'v' ) {		/* video */
		$_DST_IMG = $_SRC_IMG;
		$img_xy = $_Image_Rez[$_IMG_T];
	} else {
		$_DST_IMG = $_DST_PTH."/$_S-$_IMG_N$_IMG_X";
	}
}

/*
echo "_F: $_F<br>";
echo "_S: $_S<br>";
echo "_IMG_T: $_IMG_T<br>";
echo "ssiz: ".$_Image_Rez[$_S]."<br>";
echo "img_xy: ".$img_xy."<br>";
echo "S_IMG: $_SRC_IMG<br>";
echo "D_IMG: $_DST_IMG<br>";
*/

if ( is_dir( $_SRC_IMG) || !file_exists( $_SRC_IMG ) ) {
	echo "missing file!<br>";
}

/* if file exists, output it, else resize */
#if ( $_S>0 ) {
if ( !file_exists( $_DST_IMG ) && !file_exists( "$_DST_IMG.l" ) ) {
	/* want to create path, and generate DST_IMG */
	mkdir_p( $_DST_PTH, '0775' );

	img_resize( $_SRC_IMG, $_DST_IMG, $img_xy, $img_xy, $_IMG_TXT );
}
else {
	/* told only to generate image, but has already been done, exit */
	if ( $_G != '' ) { die; }
	
	/* some process is generating our output image, wait a while */
	while ( file_exists( "$_DST_IMG.l" ) ) {
		sleep(1);
	}
}
#}

/* update view count for image if we know db_id */
if ( $_DBID!='' && $img_xy>300 && $session['userid']>0) {
	db_media_update( $db, $_DBID, $img_xy, $_IMG_UID, $session['userid'] );
}
	
/* dump image information out */
$oldcache = ini_set( "session.cache_limiter", "private" );

image_content_type( $_DST_IMG, $_IMG_X );
readfile( $_DST_IMG );
#echo "readfile( $_DST_IMG )<br>";

ini_set( "session.cache_limiter", $oldcache );

die;
// --- END: main() ---

function db_media_update( $db, $img_id, $size, $img_uid, $sess_uid )
{
	global $HTTP_SERVER_VARS;

	// image view counter
	$sql = "UPDATE image SET views=views+1 WHERE id=$img_id";
	if ( $sess_uid>0 ){ $sql .= " AND user_id!=$sess_uid"; }
	$db->query( $sql );

	// image_user view counter
	if ( $sess_uid>0 && $sess_uid!=$img_uid ) {
		$sql = "INSERT into image_view SET"
			." image_id=$img_id,res='$size x',user_id=$sess_uid"
			.",remote_addr='".$HTTP_SERVER_VARS['REMOTE_ADDR']."'"
			;
		$db->query( $sql );
	}
//	echo "mupdate: $img_id, $size, $img_uid, $sess_uid<br>";
}
	

/** Get info about media from passed record ID
*
*/
function db_media_get( $db, $img_id )
{
	global $session;

	$sql = "SELECT i.*,u.email,DATE_FORMAT(i.cd,'%Y') AS year FROM image i"
		." LEFT JOIN user u on i.user_id=u.id"
		." WHERE i.id=$img_id AND (status='a' "
		.($session['userid']!='' ? "OR user_id=".$session['userid'] : "")
		.")";
	$db->query( $sql );
	$db->next_record();

	return $db->Record;
}

/** Create directory heirarchy ala 'mkdir -p'
*
*/
function mkdir_p($target, $mod)
{
	if ( is_dir( $target ) ){ return 1; }
	if (file_exists($target) && !is_dir($target))
		return 0;                                // failure
	if (substr($target, 0, 1) != '/')
		$target = "/".$target;          // prepend a slash
	if (ereg('\.\.', $target) || ereg("[^a-zA-Z0-9\._\-\/]", $target))
		return 0;                                // if illegal dirname
	if (@mkdir($target))                    // if new dir created,
		return 1;                                // we are successful

	if (mkdir_p(substr($target, 0, (strrpos($target, '/'))),$mod) == 1)
		return (mkdir_p($target,$mod));
	else
		return 0;
}

/**
*
*/
function img_resize( $fin, $fout, $maxwidth, $maxheight=0, $txt='' )
{
	if ( !is_file($fin) ) { return 1; }

	touch( "$fout.l" );

	$src_size = @getimagesize($fin,$iinfo);
	if ( $maxwidth==0 ){ $scale=1; }
	else {
		$scale = min($maxwidth/$src_size[0], $maxheight/$src_size[1]);
	}

	$dst_w = intval($src_size[0]*$scale);
	$dst_h = intval($src_size[1]*$scale);
	
	if ( $txt!='' ) {
		$copy = "-pointsize 12 -fill white"
			." -font helvetica -draw \"text 4,".($dst_h-4)." '$txt'\"";
		$comnt = "-comment \"$txt\"";
	}
	
	$cmd  = "convert";
	$cmd .= " -quality 80"; 
	$cmd .= " -size ".$dst_w."x$dst_h -resize $dst_w"."x$dst_h";
	$cmd .= " -interlace plane";
//	$cmd .= " +profile";
	if ( $dst_w > 200 ){ $cmd .= " $copy"; }
    $cmd .= " $comnt";
	$cmd .= ' '.escapeshellarg($fin).' '.escapeshellarg($fout);

//	echo "size: $dst_w/$dst_h<br>";
//	echo "cmd: $cmd<br>";
	exec ( $cmd );
/*	
	// grab file contents
	$fd = @fopen($fin,'r');
	$img_str = fread( $fd,filesize($fin) );
	fclose( $fd );
	
	$im = ImageCreateFromString($img_str);
	
	#$img_w = ImageSX($im);
	#$img_h = ImageSY($im);

	#$scale = $maxwidth / $img_w;
	#$nim_w = intval( $maxwidth );
	#$nim_h = intval( $img_h * $scale );
	
	$nim = imagecreatetruecolor( $nim_w, $nim_h );
//	imageantialias( $nim, true );

	imagecopyresampled( $nim,$im, 0,0,0,0,
	$nim_w, $nim_h, $img_w, $img_h );

// save and cleanup
//	imagejpeg( $nim );
	imagejpeg( $nim, $fout, 80 );
	imagedestroy( $im );
	imagedestroy( $nim );
*/
	unlink( "$fout.l" );
}

/**
*
*/
function image_content_type( $f_image, $ext='' )
{
	global $_Mime_List;

	$img_t = exif_imagetype( $f_image );
	if ( $img_t != '' ) {
		$img_mime = image_type_to_mime_type( $img_t );
	}
	else {
		$img_mime = $_Mime_List[$ext];
	}
	Header( "Content-type: $img_mime" );
//	echo "content-type: $ext, $img_t = $img_mime<br>";
}
