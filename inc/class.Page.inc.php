<?php # $Id: class.Page.inc.php,v 1.1 2002-10-29 22:16:27 paulmcav Exp $

	IncludeObject('Template');
	
	/** base class for a web page
	*
	*/
	class Page
	{
		var $t;
		var $t_name;

		function Page($name,$policy='remove')
		{
			$this->t_name= $name;
			$this->t = new Template( 'templates',$policy );
			
			# load main file, and grab body
			$this->t->set_file( $name, $name.'.html' );
			$this->t->set_block( $this->t_name, 'body' );
			
			$this->t->set_file( 'common', 'common.html' );
			
			$this->t->set_block( 'common', 'search_form' );
			$this->t->set_block( 'common', 'logout_link' );
			$this->t->set_block( 'common', 'login_link' );
		}

		function display()
		{
			global $session;

			# if the user has logged in...
			if ( $session['userid'] )
			{
				$this->t->set_var( 'login_link', '' );
				$this->t->set_var( 'user_name', $session['userem']);
			}
			else
			{
				#$this->t->set_var( 'search_form', '' );
				$this->t->set_var( 'logout_link', '' );
			}
			
			$this->t->parse('out',$this->t_name);
			
			$this->t->parse('out','common');
			$this->t->p('out');
		}
	}

