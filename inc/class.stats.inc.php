<?php # $Id: class.stats.inc.php,v 1.4 2003-07-10 20:43:05 paulmcav Exp $

	class stats
	{
		var $User_count;
		var $Host_count;

		var $db;

		function stats($dbobj)
		{
			global $session;

			$this->User_count = 0;
			$this->Image_count = 0;
			$this->Book_count = 0;

			$this->db = $dbobj;

			$this->db->query(
				   	"SELECT COUNT(1) FROM image i LEFT JOIN site_image si ON"
					." i.id=si.image_id WHERE status='a' AND"
					." si.server_id=".$session['srv_id']
				   	);
			$this->db->next_record();
			$this->Image_count = $this->db->Record[0];
			
			$this->db->query(
				   	"SELECT COUNT(1) FROM book"
				   	);
			$this->db->next_record();
			$this->Book_count = $this->db->Record[0];
			
			$this->db->query(
				   	"SELECT DISTINCT(user_id) FROM image i LEFT JOIN"
					." site_image si ON i.id=si.image_id WHERE"
					." si.server_id=".$session['srv_id']
				   	);
			$this->User_count = $this->db->num_rows();

			$this->db->free();
		}
	}
