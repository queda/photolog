<?php # $Id: class.View.inc.php,v 1.59 2004-04-17 03:25:08 paulmcav Exp $

	IncludeObject('Page');

# 410 / 640 / 800 / 1024 / 1600

	/** View book page
	*
	*/
	class View extends Page
	{ 
		var $db;

		# constructor
		function View()
		{
			global $session, $globs;
			
			$book_data =& $session['book_data'];

			// looks like we have some session data to use (maybe)
			if ( $_REQUEST['uid']==0 && $_REQUEST['st']==0 
					&& $_REQUEST['bid']!='' ) {

				$_REQUEST['uid'] = $book_data['uid'];
#				$_REQUEST['st'] = $book_data['bst'];
			}

			if ( $_REQUEST['bid']==''
#				   	|| $_REQUEST['uid']=='' || $_REQUEST['st']==''
					) {
				header( "Location: ".$globs['baseurl'] );
				die;
			}
			if ( $_REQUEST['ds']!='' ) {
				unset( $session['book_data'] );
			}

			if ( $_REQUEST['uid']!='' ) $book_data['uid'] = $_REQUEST['uid'];
					
			$this->Page('View',$globs['tpl_vars']);
			
			$this->t->set_block('body','data_no');
			$this->t->set_block('body','data_yes');

			$this->t->set_block('data_yes','book_row','book_r');
			$this->t->set_block('book_row','book_data','book_d');

			$this->t->set_block('data_yes','image_list','image_l');
			$this->t->set_block('image_list','image_row','image_r');
			$this->t->set_block('image_row','image_data','image_d');

			$this->t->set_block('data_yes','image_view','image_v');
			
			$this->db = $globs['db'];

			$this->t->set_var("java_script","");
			$this->t->set_var("java_onload","");
			
			unset($session['jscript']);
			unset($session['jload']);
			unset($session['image_row']);
			
			// check to see if we should support forms
			if ( $_REQUEST['uid'] == $session['userid'] ) {
				$session['form_data']['_uid'] = $session['userid'];
				
			}
			else {
				unset( $session['form_data'] );
				
				$_book_sel_list = '';
				$_form_extra_subs = '';
			}

			// handle form actions
			if (isset($session['form_data']['_uid']) ) {
				if ( isset($_REQUEST['fc_ta']) ) {
					$_view_message = $this->form_toggle_access();
				}
				else if ( isset($_REQUEST['fc_atb']) ) {
					$_view_message = $this->form_add_to_book(
							$_REQUEST['book_id'],$_REQUEST['uid'],
							$_REQUEST['fc_atb']);
				}
				else if ( isset($_REQUEST['fc_upd']) ) {
					$_view_message = $this->form_update_img();

					if ($_REQUEST['fc_upd'] == "Upd Nxt") {
						header( "Location: $PHP_SELF?".$_REQUEST['f_nxtlnk'] );
						die;
					}
				}
			}
			else {
				$this->t->set_block('image_list','f_image_header','fimg_h');
				$this->t->set_var('fimg_h','');
			}
			if ( isset($_REQUEST['fc_cm']) ){
				$_view_message .= $this->form_insert_cmnt($_REQUEST['cmnt']);
			}
			
			$img_size_sel = $this->set_image_size( $_REQUEST['sz'],
					$_REQUEST['uid'],$_REQUEST['bid'],$_REQUEST['st'],
					$_REQUEST['img'] );
			
			#  Get book info by dir
			$img_list = array();
			if ( $_REQUEST['st']=='d' && $_REQUEST['bid'] != '' ) {
				$this->view_books_dir($_REQUEST['uid'],$_REQUEST['bid'],
					$_REQUEST['img'],$img_list);
				$session['book_data']['cur_name'] = $_REQUEST['bid'];
			}
			// get book info from db
			else {
				$this->view_books_db($_REQUEST['bid'],$_REQUEST['img']);
			}

			$cnt = $this->view_image($_REQUEST['bid'],$_REQUEST['img'],
					$img_list);

			// set up our form information
			$_form = "<FORM name=\"imageList\" action=\"?"
				.$_SERVER['QUERY_STRING']
				."\" method=\"POST\">"; 

			// find out if we want to support some kind of management
			if ( isset($session['form_data']['_uid']) ) {

				$session['form_data']['cnt'] = $cnt;
				$_book_sel_list = $this->books_get_list($_REQUEST['uid']);
			}
			else {
				$this->t->set_block('data_yes','form_action','form_a');
				$this->t->set_var('form_a','');
			}
			
			$uid = $_REQUEST['uid'];
			$bid = $_REQUEST['bid'];
			if ( $_REQUEST['img']!=0 ){
			   $session['book_data']['cur_name']
				   = $session['image_row']['dir']
				   .":".$session['image_row']['base_name'];
			}
			$title = "Usr#$uid "
				.($_REQUEST['st']!='d' ? "Book" : "Dir")
				." Photos (".$session['book_data']['cur_name'] .")"
						;
			
			# fill out some more form variables
			$var = Array(
				'page_title' => $title,
				'view_message' => "Select Book / Image: <i>"
									.$session['book_data']['cur_name']."</i>"
									.$_view_message,
				'img_size_sel' => $img_size_sel,
				'form' => $_form,
				'book_sel_list' => $_book_sel_list,
				'java_script' => $session['jscript'],
				'java_onload' => $session['jload'],
				'url_books' => 
					($session['book_data']['uid'] 
					 	? "page=Book&st=d&uid=".$session['book_data']['uid']
						: "page=Index"),
#				'form_extra_subs' => $_form_extra_subs,
				
			);
			$this->t->set_var($var);

			unset($session['jscript']);
			unset($session['jload']);
			unset($session['refurl']);

			$_SESSION['session'] = $session;
		}

		/**
		 *
		*/
		function set_image_size( $sz,$uid,$bid,$bst,$iid )
	   	{
			global $session,$globs;

			if ( $session['userid']<1 ) {
				return "<FONT size=\"-1\">Login for HQ images.</FONT>";
			}

			$book_data =& $session['book_data'];
			$max = sizeof($globs['imagerez'])-$globs['imagexnm'];

			if ( $sz=='' ) { $sz = $session['uimg_size']; }
			if ( $sz=='' || $sz <0 || $sz >$max ) { $sz=$globs['defltrez']; }

			$iss = "Image Size: [ ";
			for ($i=0;$i<=$max;$i++) {
				$sqry = "page=View&uid=$uid&st=$bst&bid=$bid&img=$iid&sz=";
				$iss .= ($i>0 ? "|":"")
					.($i==$sz ? "<b>":'<a href="?'.enc64($sqry.$i).'">')
					.($max-$i)
					.($i==$sz ? "</b>":"</a>")
					;
			}
			$iss .=  " ]";

			$session['uimg_size'] = $sz;

			// update database w/ selected res
			if ( $sz < 6 ) {
			}
			
			return $iss;
		}

		function gen_p_book_url( $id,$name,$_bid,$_iid,$_uid,$_st )
		{
			$sqry = enc64("page=View&uid=$_uid".
					($_st!='' ? "&st=$_st" : "")
					."&bid=$id" );
			
			if ( $_bid==$id && $_iid=='') {
				if ( $name!='' )
					$buff = "<FONT size=\"+1\"><b>/$name</b></FONT>";
				else 
					$buff = "/";
			}
			else if ( $_bid==$id && $_iid=='') {
				$buff = "<FONT size=\"+1\"><b>/<a href=\"?$sqry\">$name</a></b></FONT>";
			}
			else {
				$buff = "/<a href=\"?$sqry\">$name</a>";
			}
			return $buff;
		}

		function gen_c_book_url( $id,$name,$_bid,$_iid,$_uid,$_st )
		{
			$sqry = enc64("page=View&uid=$_uid".
					($_st!='' ? "&st=$_st" : "")
					."&bid=$id$name" );

			if ( $_bid=="$id$name" && $_iid=='' ) {
				$buff = "<FONT size=\"+1\"><B>/$name</B></FONT>";
			}
			else if ( $_bid=="$id$name" || $_REQUEST['ds']==$name ) {
				$buff = "<FONT size=\"+1\"><B>/<a href=\"?$sqry\">$name</a></B></FONT>";
			}
			else {
				$buff = "/<a href=\"?$sqry\">$name</a>";
			}
			return $buff;
		}

		function gen_nxt_prv($cur_l,$np_lpfx,$list,&$nxt_prv )
		{
			$p = $n = -1;
			for ($i=0;$i<sizeof($list);$i++) {
				if ( $cur_l == $list[$i] ) {
					$p = $i-1;
					$n = $i+1;
					break;
				}
			}
			if ( $n>=sizeof($list) ) $n = -1;

			if ( $p>=0 )
				$nxt_prv[0] = $np_lpfx.$list[$p];
			if ( $n>=0 )
				$nxt_prv[1] = $np_lpfx.$list[$n];

			return $i;
		}
		
		/**
		 *
		*/
		function view_books_dir($u_id,$b_id,$i_id,&$ndb_img_list)
	   	{
			global $session,$globs;

			$image_dir = $globs['imagedir'];

			// check to see if a session var is set
			if ( isset($session['book_data']) ) {
				$book_data =& $session['book_data'];
			}
			else {
				$book_data['lid'] = 0;
			}

			$book_data['ptre'][0] = array();
			$book_data['ptre'][1] = array();

# list of found images in the directory
			$ndb_img_list = array();
			
			$b_url = $b_id;

			if ( $b_id=='all' ){ $b_url = ''; }

			// remove pre && trailing '/'
			if ($b_url[0]=='/')
				$b_url = substr($b_url,1);
			if ($b_url[strlen($b_url)-1]=='/')
				$b_url = substr($b_url,0,-1);

			$path_parts = explode('/',$b_url);

			for ($level=0;$level<sizeof($path_parts);$level++){
				$book_data['ptre'][0][$level] =
					(($level>0) ? $book_data['ptre'][0][$level-1].'/':'')
					.$path_parts[$level];
				$book_data['ptre'][1][$level] = $path_parts[$level];

#echo "lst: ".$book_data[lst][$level]."<br>";
			}
			$level--;

			// TODO
			// problem here when session value is different than
			// the passed b_id (b_url) value.
			// ... should probably do a check, and then revert to the
			// previous directory to ensure syncronization.

			// list of directories not in session / refresh list
			if ( $book_data['lid'] != $b_url || $b_url=='') {

/*				
echo "b_url: $b_url, lid: $book_data[lid]<br>";

				// Find where lid != b_url
				$l_ids = explode('/',$book_data['lid']);
				$c_ids = explode('/',$b_url);
				$n = min(count($l_ids),count($c_ids));

				$d_url = "";
				
				if ($n>1){ $n--; }

				for ($p=0;$p<$n;$p++) {
				   	echo "p: $p = $l_ids[$p] / $c_ids[$p]<br>";
#					
					$d_url .= ($d_url!='' ? '/':'').$c_ids[$p];
#					if ($l_ids[$p] != $c_ids[$p]) { continue; }

			   	}
				$b_url = $d_url;

	echo "b_url: $b_url<br>";
				
#				$d_url = $b_url;

	echo "image_dir: $image_dir/$u_id/$b_url<br>";
*/
				$dir_list = array();
				if ($dir = @opendir("$image_dir/$u_id/$b_url")) {
					unset( $session['add_images'] );
					while (($file = readdir($dir)) != false) {
						// track images in the dir
						$ext = strtolower(substr($file,-3));
						if ( $globs['mimelst'][$ext]!='' && $ext!='thm' ){
							$ndb_img_list[$file] = $b_url;
						}
						if ( $file[0]=='.'
							|| !is_dir("$image_dir/$u_id/$b_url/$file")
								)
							continue;
						array_push($dir_list,$file);
#	echo "dir: $file<br>";						
					}
				}
				sort($dir_list);

				// only use new dirlist if found any subdirs
				if (sizeof($dir_list)) {
#	echo "dir_list: $dir_list[0]<br>";
					$book_data['cbids'] = $dir_list;
				}
				// one of the lowest subdirs was selected... get nxt / prv
				else {
					$level--;
					unset( $book_data['np_bid'] );

					$book_data['np_bi'] = $this->gen_nxt_prv(
							$book_data['ptre'][1][$level+1],
							$book_data['ptre'][0][$level].'/',
							$book_data['cbids'],
							$book_data['np_bid']
							);
				}
			}
			else {
				$level = $book_data['lvl'];

			}
			$root = $book_data['ptre'][0][$level];
			if ( $root!='' ){ $root .= '/'; }
			
			// track current session position
			$book_data['lid'] = $b_url;
			$book_data['uid'] = $u_id;
			$book_data['bst'] = 'd';
			$book_data['lvl'] = $level;

#			$session['book_data'] = $book_data;
			
			// add the 'all' path
			array_unshift($book_data['ptre'][0], 'all');
			array_unshift($book_data['ptre'][1], 'all');
			$level++;
			
			// expand parent links
			$url = "";
			for ($i=0;$i<=$level;$i++) {
				$url .= $this->gen_p_book_url(
						$book_data['ptre'][0][$i], $book_data['ptre'][1][$i],
						$b_url, $i_id, $u_id, 'd' );
			}

			$var = array( "book_link" => $url );
			$this->t->set_var($var);
			$this->t->parse('book_d','book_data','true');
			$this->t->parse('book_r','book_row','true');

			$this->t->set_var('book_d','<td></td>');
					
			$len = 0;
			// expand child links
			for ($i=0;$i<sizeof($book_data['cbids']);$i++) {
				$len += strlen($book_data['cbids'][$i]);
				$url = $this->gen_c_book_url( $root,
						$book_data['cbids'][$i],
						$b_url, $i_id, $u_id, 'd' );
				
				$var = array( "book_link" => $url);
				$this->t->set_var($var);
				$this->t->parse('book_d','book_data','true');
				// wrap at string length || do a mod on num cols.
				if ( $len>=70 ) {
					$len = 0;
#				if ( (($i+1)%12)==0 ){
					$this->t->parse('book_r','book_row','true');
					$this->t->set_var('book_d','<td></td>');
				}
			}
#			if ( (($i+1)%12)!=0 )
			if ( $len != 0 )
				$this->t->parse('book_r','book_row','true');
		}

		/**
		 *
		*/
		function view_books_db($b_id,$i_id)
	   	{
			global $session,$globs;

			$image_dir = $globs['imagedir'];
			$b_url = $b_id;

			// check to see if a session var is set
			if ( isset($session['book_data']) ) {
				$book_data =& $session['book_data'];
			}
			else {
				$book_data['lid'] = 0;
			}

			$u_id = $book_data['uid'];
#			$book_data['ptre'] = array();

#			if ( $b_id=='all' ){ $b_url = ''; }

			// get top level book information
			$sql = "SELECT *,b.id bid,DATE_FORMAT(cd,'%d%b%Y') _cd FROM book b"
				  ." WHERE status IS NOT NULL AND"
				  ." id=$b_id or p_id=$b_id ORDER BY p_id,id";
			
			$this->db->query( $sql );

			$level=0;
			$tpl_data = "";
			$opid = -1;
			while( $this->db->next_record() ) {
				$row = $this->db->Record;

				if ($level==0){
					$session['book_data']['cur_name'] = $row['name'];
				}
				if ( $opid==-1 ) $opid = $row['p_id'];

#	echo "name($opid): ($row[id]) $row[name]<br>";
	
				$book_data['ptre'][0][$level] = $row['bid'];
				$book_data['ptre'][1][$level] = $row['name'];

				$url = $this->gen_p_book_url(
						$row['bid'], $row['name'],
						$b_url, $i_id, $u_id, '' );
						
				$var = array( "book_link" => $url );
				$this->t->set_var($var);
				$this->t->parse('book_d','book_data','true');

				if ( $orid != $row['bid'] ){
					$orid = $row['bid'];
					$this->t->parse('book_r','book_row','true');
					$this->t->set_var('book_d','<td></td>');
				}
#				$this->t->parse('book_r','book_row','true');
				
				$level++;
			}
			$level--;


/*			// get any child books
			$sql = "SELECT id,p_id,name FROM book"
				  ." WHERE status IS NOT NULL AND"
				  ." p_id=$b_id ORDER BY p_id,id";
			
			$this->db->query( $sql );

			$level=0;
			while( $this->db->next_record() ) {
				$level++;
				$row = $this->db->Record;
	echo "name2: ($row[id]) $row[name]<br>";

				$book_data['lst'][$level]  = $row['id'];
				
				array_push($dir_list,$row['name']);
			}
*/			
			// track current session position
			$book_data['lid'] = $b_url;
			$book_data['uid'] = $u_id;
			unset( $book_data['bst'] );
			$book_data['lvl'] = $level;

#			$session['book_data'] = $book_data;
			
		}
			
		/**
		 *
		*/
		function view_image($b_id,$i_id,$ndb_img_list)
	   	{
			global $session, $globs;

			// session data
			$book_data =& $session['book_data'];

			$u_id = $book_data['uid'];
			$stat = $book_data['bst'];

			// looking at directory mode
			if ( $i_id > 0 ) {
				$sql = "SELECT i.*,i.id iid,si.*,si.id siid,DATE_FORMAT(cd,'%d%b%y %h:%i') nicedate"
					." FROM image i"
					." LEFT JOIN site_image si ON i.id=si.image_id"
				    ." WHERE i.id=$i_id"
					." AND si.server_id=".$session['srv_id']
					." AND (status='a'"
					. ($session['userid']>0
						? " OR i.user_id=".$session['userid'] : "")
					.") ORDER BY media,name";

				// find next / prev image
				if ( isset($book_data['imgs']) ){
					unset( $book_data['np_iid'] );

					$book_data['np_ii'] = $this->gen_nxt_prv(
							$i_id, "",
							$book_data['imgs'],
							$book_data['np_iid']
							);
				}
			}
			
			// want to find a list of images
			if ( $b_id != '' ) {
				// no selected image
				if ( $i_id=='' ) {
					// directory mode
					if ( $stat=='d' ) {
#						$sql = "SELECT *,DATE_FORMAT(cd,'%d%b%y %h:%i') nicedate FROM image i"
#							." WHERE user_id=$u_id AND dir='/"
#							.addslashes($b_id)."'";
						$sql = "SELECT i.*,i.id iid,si.*,si.id siid,DATE_FORMAT(cd,'%d%b%y %h:%i') nicedate"
							." FROM image i"
					  		." LEFT JOIN site_image si ON i.id=si.image_id"
							." WHERE i.user_id=$u_id"
							." AND si.server_id=".$session['srv_id']
							." AND dir='/" .addslashes($b_id)."'";
					}
					// selcted book id
					else {
						$sql = "SELECT *,bi.image_id iid,DATE_FORMAT(cd,'%d%b%y %h:%i') nicedate"
							." FROM book_image bi"
							." LEFT JOIN site_image si ON bi.image_id=si.image_id"
						    ." LEFT JOIN image i ON si.image_id=i.id"
							." WHERE bi.book_id=$b_id"
							." AND si.server_id=".$session['srv_id']
							;
#			echo "sql: $sql<br>";
					}
					$sql .= ($session['form_data']['_uid'] ? ""
									:" AND status='a'")
							." ORDER by media,cd";
				}
			}
			$img_list = array();

#  echo "sql: $sql<br>";
			// get images for selected book / path
			$this->db->query( $sql );

			$r = $c = $cnt = 0;
			$max_col = $globs['max_cols'];
#			$max_col['i'] = 8;	// 85px
#			$max_col['v'] = 4;	// 160px
			$pmt = '';
			$fcs = 0;
			while ( $this->db->next_record() ) {
				$row = $this->db->Record;

				// image is in database, remove it from the non-db list
				unset( $ndb_img_list[$row['name']] );
				
				array_push($img_list,$row['iid']);

				// view details
				if ( $i_id == $row['iid'] ){
					$this->image_data_detail( $row, $book_data, $i_id );
					$this->t->parse('image_v','image_view','true');
				}
				else {
					$this->image_data_thumb( $row, $book_data,$cnt );
#					$this->t->set_var('image_v','');
				}

				$row['kbytes'] = sprintf("%.2f", $row['size'] / 1024);
				$this->t->set_var( $row );

				// looking at single image
				if ( $i_id == $row['iid'] ){
#					$this->t->parse('image_v','image_view','true');
				}
				// looking at thumb images
				else {
#					$this->t->set_var('image_v','');
					if ( $pmt != $row['media'] && $pmt != '' ){
						// TODO problems with start and range.
						if ( isset($session['form_data']['_uid']) ) {
							$_f_row_sel = '<td bgcolor="#aaaaff">'
								.'<A href="#" onClick="checkEvery('
								."'f_img',".($cnt+1-$c).",1,$c); return false;\">"
								.'&gt;</a></td>';
							$this->t->set_var('f_row_sel', $_f_row_sel );
						}

						if ( $c != 0 )
							$this->t->parse('image_r','image_row','true');
						$this->t->parse('image_l','image_list','true');
						$this->t->set_var('image_r','');
						$this->t->set_var('image_d','');
						$c = 0;
						$this->t->set_var('f_col_sel','');
#						$fcs = 0;
					}

					$this->t->parse('image_d','image_data','true');
					
					// form col headers
					if ( isset($session['form_data']['_uid']) 
							&& $fcs < $max_col[$row['media']] ) {
						
						$_f_col_sel .= '<td bgcolor="#aaaaff">'
							.'<A href="#" onClick="checkEvery('
							."'f_img',$fcs,".$max_col[$row['media']].",0);"
							." return false;\">v</A></td>\n";
						$fcs++;
						$this->t->set_var('f_col_sel',$_f_col_sel);
					}
					if (++$c == $max_col[$row['media']]) {
						
						if ( isset($session['form_data']['_uid']) ) {
							$_f_row_sel = '<td bgcolor="#aaaaff">'
								.'<A href="#" onClick="checkEvery('
								."'f_img',".($cnt+1-$c).",1,$c); return false;\">"
								.'&gt;</a></td>';
							$this->t->set_var('f_row_sel', $_f_row_sel );
						}

						$this->t->parse('image_r','image_row','true');
						$this->t->set_var('image_d','');
						$c = 0;
#						$_f_row_sel = '';
					}
					$pmt = $row['media'];
				}
				$cnt++;
			}

			// if we have some images in dir, but not in DB
			if ( count($ndb_img_list) ){
				ksort($ndb_img_list);
				$session['add_images'] = $ndb_img_list;
#	reset( $ndb_img_list );
#	foreach( $ndb_img_list as $k => $v )
#		echo "img: $k = $image_dir/$u_id/$v<br>";
			}
			if ( isset($session['add_images']) &&
				   	$session['userid']==$u_id
					)
				$_add_images = '<a href="?'
					.enc64('page=Add').'">Add '
					.count($session['add_images'])
					." new</a> images.";

			// looking at thumb images
			if ( $i_id != $row['iid'] ){
				// TODO problems with start and range.
				if ( isset($session['form_data']['_uid']) ) {
					$_f_row_sel = '<td bgcolor="#aaaaff">'
						.'<A href="#" onClick="checkEvery('
						."'f_img',".($cnt+1-$c).",1,$c); return false;\">"
						.'&gt;</a></td>';
					$this->t->set_var('f_row_sel', $_f_row_sel );
				}
				if ( $c!=0 )
					$this->t->parse('image_r','image_row','true');
				$this->t->parse('image_l','image_list','true');
				$this->t->set_var('image_v','');
			}
			
			if ( sizeof($img_list)>1 ){
				$book_data['imgs'] = $img_list;
			}
			else {
			}
#			$session['book_data'] = $book_data;

			// prev book
			if ( isset($book_data['np_bid'][0]) ){
				$sqry = enc64("page=View"
						."&uid=".$book_data['uid']."&st=".$book_data['bst']
						."&bid=".$book_data['np_bid'][0]);
				$_prev_book = "<a href=\"?$sqry\">&lt;&lt; PrvBk</a>";
				$_prev_book .= " (".$book_data['np_bi'].")";
			}
			// next book
			if ( isset($book_data['np_bid'][1]) ){
				$sqry = enc64("page=View"
						."&uid=".$book_data['uid']."&st=".$book_data['bst']
						."&bid=".$book_data['np_bid'][1]);
				$_next_book = "(".(sizeof($book_data['cbids'])-$book_data['np_bi']-1).") ";
				$_next_book .= "<a href=\"?$sqry\">NxtBk &gt;&gt;</a>\n";
			}
			
			// specific image view
			if ($i_id!='') { 
				$var = array(
					'pb_ops' => 'width="16%"',
					'pi_ops' => 'width="16%"',
					'bn_ops' => 'width="36%" class="title_bg"',
					'ni_ops' => 'width="16%"',
					'nb_ops' => 'width="16%"',
					'prev_book' => ($_prev_book!='' ? $_prev_book : "&nbsp;"),
					'next_book' => ($_next_book!='' ? $_next_book : "&nbsp;"),
					);
			}
			else {
				$var = array(
#					'f_col_sel' => $_f_col_sel,
#					'f_row_sel' => $_f_row_sel,
					'pb_ops' => '',
					'pi_ops' => '',
					'bn_ops' => '',
					'ni_ops' => '',
					'nb_ops' => '',
					'add_images' => $_add_images,
					'base_name' => '',
					"prev_book" => $_prev_book,
					"next_book" => $_next_book,
						);
			}
			$this->t->set_var($var);
			
			return $cnt;
		}

		/** Display image thumbnail
		 *
		*/
		function image_data_thumb( $row, $book_data, $num )
	   	{
			global $session, $globs;
			
#			echo "row: <pre>"; print_r($row); echo "</pre>";
			// should probably just use $globs['imagerez']
			$ssiz = $globs['imagerez'];
//			$ssiz = array( 'i'=>85, 'p'=>160, 'v'=>160 );

			$b_id  = $book_data['lid'];
			$bname = substr($row['name'],0,strrpos($row['name'],'.'));

			$_form_data = '';

			// view a thumbnail
			$_img_url = enc64("page=View&uid=".$book_data['uid']
					."&st=".$book_data['bst']
					."&bid=$b_id&img=".$row['iid']);

			$dims = explode('x',$row['res']);
			
			$iscale = @min($ssiz[$row['media']]/$dims[0],
							$ssiz[$row['media']]/$dims[1]);

			$iw = (int)($dims[0]*$iscale);
			$ih = (int)($dims[1]*$iscale);
						  
			$img_base = "media.php?";
//			$qry_str = "s=0&f=".$row['user_id'].'/'.$row['dir'];
			$qry_str = "s=0&f=dat/".$row['user_id'].$row['dir'];
			
			if ($row['media']!='v') {
//				$qry_str .= urlencode("/.thumbnails/".$row['name'].".png");
				$qry_str .=  urlencode("/".$row['name'].".png");
			}
			elseif ($row['media']=='v') {
				$qry_str .= urlencode("/$bname.thm");
			}
			
			$img_url = $img_base.enc64($qry_str);
			$_img_args = "width=\"$iw\" height=\"$ih\" src=\"$img_url\""
					." title=\"$bname"
					.($row['title']!=''? " ~ ".$row['title'] : "")
					."\"";
				;

			// add some form information
			if ( $session['form_data']['_uid'] ) {
				$_form_data .=
					"<INPUT type=\"checkbox\" name=\"f_img[$num]\""
					." value=\"".$row['iid']."\">";
			}
			
			if ( $row['status']!='a' ) {
				$bname = "<strike>$bname</strike>";
			}

			if ( $session['form_data']['_uid'] ) {
				$_n_views = "<font color=grey>["
					."<a href=\"#\" onClick=\"_openBrWindow('?page=I_Views&id=".$row['iid']
					."','views',400,300,'scrollbars=yes,resizable=yes');"
					." return false;\">".$row['views']."</a>]</font> ";
			}
			else {
				$_n_views = '';
			}
			
			$var = array(
					"n_views" => $_n_views,
					"base_name" => $bname,
					"image_url" => $_img_url,
					"img_args" => $_img_args,
					"form_data" => $_form_data,
					);
			$this->t->set_var($var);
		}
			
		/** Display image detail(s)
		 *
		*/
		function image_data_detail( $row, $book_data, $i_id )
	   	{
			global $session, $globs;

			$ssiz  = $globs['imagerez'];
			$b_id  = $book_data['lid'];
			$bname = substr($row['name'],0,strrpos($row['name'],'.'));
			
			// grab our view size flag
			$sflg = $session['uimg_size'];
			if ( $sflg=='' || $sflg <0 || $sflg
					>=sizeof($ssiz)-$globs['imagexnm']+1 )
				$sflg = $globs['defltrez'];
			
			$url = "";

			if ( $session['userid']<1 
					|| ($sflg==(sizeof($globs['imagerez'])-$globs['imagexnm']))
			   ) {
				$sflg = $row['media'];
			}

			// view single image
			$jscript = "function idd_preLoad() {\n  ";

			// get image scaling size
			$dims = explode('x',$row['res']);
			
			// dont want to scale image >> image size
			if ( $ssiz[$sflg] >= max($dims[0],$dims[1]) )
				$sflg = 0;
					
			if ($sflg)
				$iscale = min($ssiz[$sflg]/$dims[0], $ssiz[$sflg]/$dims[1]);
			else
				$iscale=1;

			$iw = (int)($dims[0]*$iscale);
			$ih = (int)($dims[1]*$iscale);

			// prev image
			if ( isset($book_data['np_iid'][0]) ){
				$sqry = enc64("page=View"
						."&uid=".$book_data['uid']."&st=".$book_data['bst']
						."&bid=$b_id&img=".$book_data['np_iid'][0]);
				$_prev_image = "<A href=\"?$sqry#image\">&lt;&lt; </A>"
							."<A href=\"?$sqry\">PrvImg</A>";
				$_prev_image .= " (".$book_data['np_ii'].")";

				if ( $sflg>0 ) {
					$jscript .= "pimg=new Image(); pimg.src=\"media.php?"
							.enc64("g=1&s=$sflg&img="
								.$book_data['np_iid'][0])
							."\";\n  ";
				}
			}
			
			// how to deal with displaying the image.
			// TODO display a panaramic image w/ java pano viewer.
			$img_base = "media.php?";
			
# echo "sflg: $sflg, ".$row['media']." ".($sflg==$row['media'])."<br>";
			
			// has the user NOT logged in or showing thumbs?
			if ( $session['userid']<1
			   	|| ($sflg && $sflg==$row['media'])
				   	) {
				// just display login2view image message.
				if ($session['userid']<1) {
					$img_base = "img/li2vw.gif";
					$qry_str = "";
				}
				else {
					$qry_str = "s=0&f=".$row['user_id'].'/'.$row['dir'];
					
					if ($sflg=='i')
						$qry_str .= urlencode("/.thumbnails/".$row['name'].".png");
					if ($sflg=='v')
						$qry_str .= urlencode("/$bname.thm");
				}
			}
			else {
				$qry_str = "s=$sflg&img=".$row['iid'];
#						"f=".$row['dir']."/".$row['name'];
			}
			
			
			$_img_src_url1 = "";
			$_img_src_url0 = "";
			
			$img_lnk = "IMG";
			if ( $session['userid']>0 ) {
				
				if ( $session['userid'] == $row['user_id'] ) {
					$aqry_str = "s=0&d=1&img=".$row['iid'];
					$_img_src_url1="<A href=\"$img_base".enc64($aqry_str)."\">";
					$_img_src_url0="</A>";
				}
					
				// if mpeg file is there, then embed it into webpage
				$_m = 0;
				if ( file_exists( $globs['imagedir']."/".$row['user_id'].'/'
							.$row['dir']."/$bname.mpg") ) {
					$_m = 1;
					$aqry_str = "m=mpg&";
				}
				
				// not showing thumbs, and a video
				if (($sflg=='0')&&($row['media']=='v')) {

					$qry_str = "s=0&f=".$row['user_id'].'/'.$row['dir']
						."/$bname.thm";
					$aqry_str .= "s=0&d=1&img=".$row['iid'];
#						"f=".$row['dir'].'/'.$row['name'];
					
					$_img_src_url1="<A href=\"$img_base".enc64($aqry_str)."\">";
					$_img_src_url0="</A>";
					$_image_src .= $_img_src_url1;
#					echo "<Pre>"; print_r( $globs); echo "</pre>";

					if ( $_m 
							&& $globs['browser']['os'] =='WIN' 
							) {
						$img_lnk = "EMBED border=0 width=\"320\" height=\"256\"";

						$qry_str = $aqry_str;
#."&m=mpg";
					}
					else {
						$img_lnk = "IMG border=1 width=\"$iw\" height=\"$ih\" "
								."lowsrc=\"img/loading.gif\""
								." title=\"$bname"
								.($row['title']!=''? " ~ ".$row['title'] : "")
								."\"";
						$_obj_post = "";
					}
				}
			}

			$_image_src .= "<$img_lnk src=\""
				."$img_base".enc64($qry_str)."\"/>";
				
			if ($row['media']=='v' && $session['userid']>0) {
				$_image_src .= $_obj_post.$_img_src_url0;
			}

			// next image
			if ( isset($book_data['np_iid'][1]) ){
				$nxtlnk = enc64("page=View"
						."&uid=".$book_data['uid']."&st=".$book_data['bst']
						."&bid=$b_id&img=".$book_data['np_iid'][1]);
				$_next_image = "(".
					(sizeof($book_data['imgs'])-$book_data['np_ii']-1).") ";

				$_next_image .= "<a href=\"?$nxtlnk\">NxtImg</a>"
							 ."<a href=\"?$nxtlnk#image\"> &gt;&gt;</a>";

				if ( $sflg >0 ) {
					$jscript .= "nimg=new Image(); nimg.src=\"media.php?"
							.enc64("g=1&s=$sflg&img="
									.$book_data['np_iid'][1])
							."\";\n  ";
				}
			}
			$jscript .= "}\n";
			$jload   = 'idd_preLoad();';

			if ( $session['userid']<1 || $session['uimg_size']==6 ) {
				$jscript = '';
				$jload = '';
			}

			// add some form information
			if ( $session['form_data']['_uid'] ) {
				$_form_data .=
					"<INPUT type=\"hidden\" name=\"f_img[0]\""
					." value=\"".$row['iid']."\">";

				$_title = "T:<INPUT type=text name=\"d_img[0]\" size=\"50\""
					." value=\"".$row['title']."\">";
				$_notes = "N:<TEXTAREA wrap=soft cols=40 rows=2 name=\"d_img[1]\">"
					.$row['notes']."</TEXTAREA>";
				$_kwds = "K:<INPUT type=text name=\"d_img[2]\" size=\"50\""
					." value=\"".$row['kwds']."\">";
				
				$_form_extra_subs = 
					 '<INPUT type="submit" name="fc_upd" value="Update">';

				if ( isset($book_data['np_iid'][1]) ){
					$_form_extra_subs .= 
						'<INPUT type="submit" name="fc_upd" value="Upd Nxt">';
					$_form_extra_subs .=
						"<INPUT type=\"hidden\" name=\"f_nxtlnk\""
						." value=\"$nxtlnk\">";
				}
			}
			else {
				$_title = $row['title'];
				$_notes = $row['notes'];
				$_kwds  = ""; #$row['kwds'];
			}

			// get user notes
			$sql = "SELECT u.*,user_id,note,n.ts FROM image_note n LEFT JOIN"
				." user u ON n.user_id=u.id WHERE image_id=$i_id";
#	echo "sql: $sql<br>";			
			$this->db->query( $sql );
			
			$_comments = '';
			$_usr_cmnt_val = '';
			while ( $this->db->next_record() ) {
				$r_in = $this->db->Record;

				if ( $r_in['id'] == $session['userid'] ){
					$_usr_cmnt_val = $r_in['note'];
#					continue;
				}
				$_comments .= "<FONT size=-1><i>".$r_in['note']."</i> ~ "
					.$r_in['name']."</FONT><br>";
			}
			/* display comment box if user signed in */
			if ( ($session['userid']>0) && !$session['form_data']['_uid']
			   ) {
				$_cmt_sub = '<INPUT type="text" name="cmnt" value="'
					.$_usr_cmnt_val.'" size="40">'
					.'<INPUT type="submit" name="fc_cm" value="Enter Comment">';
			}
			
			if ( $row['status']!='a' ) {
				$bname = "<strike>$bname</strike>";
			}
			
			// exif formatting
			$exifdata = preg_split('/\n/',$row['exif']);
			$_exif0 = $_exif1 = $_exif2 = '';
			for ($i=0;$i<sizeof($exifdata);$i++) {
				if ( $exifdata[$i]=='' ) continue;
				$_exif0 .= $exifdata[$i]."<br>";
				if ($i%2) {
					$_exif2 .= $exifdata[$i]."<br>";
				}
				else {
					$_exif1 .= $exifdata[$i]."<br>";
				}
			}
			
			$exif_link = "_openBrWindow('?page=Info','exif',330,390,"
						."'scrollbars=yes,resizable=yes');";
			$var = array(
					"_title" => $_title,
					"_comments" => $_comments,
					"_notes" => $_notes,
					"_kwds" => $_kwds,
					"_cmt_sub" => $_cmt_sub,
					"exif0" => $_exif0,
					"exif1" => $_exif1,
					"exif2" => $_exif2,
					"image_src" => $_image_src,
					"obj_pre" => $_obj_pre,
					"obj_post" => $_obj_post,
					"img_src_url0" => $_img_src_url0,
					"img_src_url1" => $_img_src_url1,
					"prev_image" => ($_prev_image!='' ? $_prev_image : "&nbsp;"),
					"next_image" => ($_next_image!='' ? $_next_image : "&nbsp;"),
					"base_name" => $bname,
					"rez" => "$iw x $ih",
					"form_data" => $_form_data,
					"form_extra_subs" => $_form_extra_subs,
					"exif_link" => $exif_link,
					);
			$this->t->set_var($var);

			if ($session['reload_info']=='1')
				$jload .= $exif_link;

			$row['base_name'] = $bname;
			$session['image_row'] = $row;
			$session['jscript'] .= $jscript;
			$session['jload']   .= $jload;
		}

		/**
		 *
		*/
		function form_toggle_access()
	   	{
			global $session;

			$form_data = $session['form_data'];
			$f_img = $_REQUEST['f_img'];
			
			$cnt = $form_data['cnt'];

			$id_list = '0';
			$mod = 0;
			
			// find which id's to toggle access for
			for ($i=0;$i<=$cnt; $i++) {
				if ( isset($f_img[$i]) ){
					$mod++;
					$id_list .= ",".$f_img[$i];
				}
			}

			// perform query
			if  ($mod>0) {
				$sql = "UPDATE image SET status=NOT status WHERE id IN("
					  .$id_list.")";
				$this->db->query( $sql );
			}
			return "<br>&nbsp;&nbsp;Access set for $mod images.";
		}

		/**
		 *
		*/
		function form_add_to_book($book_id,$u_id,$cmd)
	   	{
			global $session, $globs;

			$form_data = $session['form_data'];
			$f_img = $_REQUEST['f_img'];

			$num = $form_data['cnt'];

			// want to add a new book
			$bk_name = trim($_REQUEST['newbook']);

#	echo "num: $num (bid=$book_id = $bk_name)<br>";

			// what command was give (add / remove)
			if ( $cmd=='Add To Book' )
				$cmd = 1;
			else if ( $cmd=='Remove' )
				$cmd = 2;
			else
				return;

			if ( $num<1 ) return;
			
			$this->db->Halt_On_Error = 'no';

			$new_book = 0;
			if ( $cmd==1 && $book_id == -1 && $bk_name!='') {
				$sql = "INSERT INTO book (user_id,name,cd)"
					." VALUES($u_id,'$bk_name',NOW())";
				
				$this->db->query( $sql );
				$book_id = $this->db->get_last_insert_id(".",".");
				$new_book = -1;
			}

			// find which id's to add into book
			$cnt = 0;
			for ($i=0;$book_id>0 && $i<=$num; $i++) {
				if ( isset($f_img[$i]) ){
					$cnt++;
					// add
					if ( $cmd==1 ) {
						$this->db->query(
							"INSERT INTO book_image (image_id,book_id)"
							."VALUES(".$f_img[$i].",$book_id)"
							);
						if ( $new_book==-1 )
							$new_book = $f_img[$i];
					}
					// remove
					else if ( $cmd==2 ) {
					}
				}
			}
			// try and update book date with 1st image date
			if ( $cmd==1 && $new_book>0 ) {
				$sql = "SELECT cd FROM image WHERE id=$new_book";
				$this->db->query( $sql );
				$this->db->next_record();
				
				$row = $this->db->Record;
				$sql = "UPDATE book set image_id=$new_book,cd='".$row['cd']
					."' WHERE id=$book_id";
				$this->db->query( $sql );
			}
			$this->db->Halt_On_Error = 'yes';
			
			return "<br>&nbsp;&nbsp;".($cmd==1 ? "Added" : "Removed")
				." $cnt image(s) to/from book.";
		}

		/**
		 *
		*/
		function books_get_list($u_id)
	   	{
			global $session;
			
			$sql = "SELECT b.*,"
				  ."SUM(IF(bi.image_id is NOT NULL,1,0)) AS img_cnt"
				  ." FROM book b"
				  ." LEFT JOIN book_image bi ON b.id=bi.book_id"
				  ." WHERE b.status LIKE '%a%' and b.status NOT like '%c%'"
				  ." AND b.user_id=$u_id"
				  ." GROUP BY b.id ORDER BY ts DESC";
			$this->db->query( $sql );

			$op_list = '';
			while ( $this->db->next_record() ) {
				$row = $this->db->Record;
				$op_list .= '<OPTION value="'.$row['id'].'">('
					       .$row['img_cnt'].") ".$row['name']."</OPTION>\n";
			}
			return $op_list;
		}
		
		/**
		 *
		*/
		function form_update_img() {
			global $session;

			$form_data = $session['form_data'];
			$f_img = $_REQUEST['f_img'];
			$d_img = $_REQUEST['d_img'];
			
			// only updating one image..
			$sql = "UPDATE image SET title='".addslashes($d_img[0])
				."',notes='".addslashes($d_img[1])
				."',kwds='".addslashes($d_img[2])
				."' WHERE id=".$f_img[0];

			$this->db->query( $sql );

			return "<br>&nbsp;&nbsp;Image information updated.";
		}
		
		/**
		 *
		*/
		function form_insert_cmnt( $_note ) {
			global $session;

			$uid = $session['userid'];
			$f_img = $_REQUEST['img'];
			
			// only updating one image..
			$sql = "REPLACE image_note SET image_id=$f_img,user_id=$uid"
				.",note='$_note'";

			$this->db->Halt_On_Error = 'no';
			$this->db->query( $sql );
			$this->db->Halt_On_Error = 'yes';

			return "<br>&nbsp;&nbsp;Image comment added.";
		}

	}

