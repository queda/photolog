<?php # $Id: class.Add.inc.php,v 1.12 2003-09-15 01:05:49 paulmcav Exp $

	IncludeObject('Page');
	
	/** Add page
	*
	*/
	class Add extends Page
	{
		var $vid_types = array( 'avi'=>'v', 'mov'=>'v');
			
		function Add()
		{
			global $session, $globs;

			$this->Page('Add');
			
			$this->t->set_block('body','image_row','image_r');
			$this->t->set_block('image_row','image_data','image_d');
#			$this->t->set_var('common','{body}');

			// if our image is set, display info for it
			if (!isset($session['add_images']) ){
				$this->t->set_var('body','');
				die;
			}
	
			$img = $session['add_images'];
			
			if ( $_REQUEST['cmd']=='' ){
				$cnt = $this->select_add_images($img);
				$message = '';
				$session['refurl'] = $refurl = $_SERVER['HTTP_REFERER'];
			}
			else {
				$cnt = count($img);
				$this->t->set_block('body','form');
				$this->t->set_var('form','');

				$message = $this->add_images($_REQUEST['cmd'],$img);

				if ( $message > 0 ) {
					$refurl = $session['refurl'];
					unset( $session['add_images'] );
					unset( $session['refurl'] );
				}
				$message .= " Images added to database";
			}
			
			$var = Array(
				'page_title' => '',
				'img_count' => $cnt,
				'message' => $message,
				'refurl' => $refurl,
				'java_onload' => '',
				'java_script' => '',
			);
			$this->t->set_var($var);
		}

		function select_add_images( $img_list )
		{
			global $session, $globs;

			$c = $cnt = 0;
			$max_cols = $globs['max_cols']['i'];

			foreach( $img_list as $k => $v ) {
				$ext = strtolower(substr($k,-3));
				$t = $this->vid_types[$ext];
				if ( $t=='v' ){
					$f = substr($k,0,-3).'thm';
					$s = 0;
				}
				else {
					$f = $k;
					$t = 'i';
					$s = 't';
				}
				$file = urlencode($session['userid'].'/'.$v.'/'.$f);
				$img_src = 'media.php?'.enc64("s=$s&f=$file");

				$tpl_data = array(
						'cnt' => $cnt++,
						'file' => $k,
						'img_path' => $v,
						'img_url' => "uid=".$session['userid']."&st=d&bid=",
						'img_src' => $img_src,
						);

				$this->t->set_var($tpl_data);
				$this->t->parse('image_d','image_data','true');
					
				if ( ++$c==$max_cols ){
					$this->t->parse('image_r','image_row','true');
					$this->t->set_var('image_d','');
					$c = 0;
				}
			}
			if ($c!=$max_cols) $this->t->parse('image_r','image_row','true');

			return count($session['add_images']);
		}

		function add_images( $cmd, $img_list )
		{
			global $session, $globs;

			$cnt = 0;

			if ( $cmd=='Selected' ) {
				if (is_null($_REQUEST['img']))
				   	return 'No images selected to add.';
				$items = $_REQUEST['img'];
			}
			else {
				$items = $img_list;
			}
			
#			$c = 0;
			foreach( $items as $k => $v ) {
				$cnt += $this->add_db_image( $k, $img_list[$k], 0 );
/*
				if ( ++$c==$max_cols ){
					$this->t->parse('image_r','image_row','true');
					$this->t->set_var('image_d','');
					$c = 0;
				}
				
			}
			if ($c!=$max_cols) $this->t->parse('image_r','image_row','true');
*/
			}
			return $cnt;
		}

		function add_db_image($img, $path, $mode=0)
		{
			global $session, $globs;

			$ext = strtolower(substr($img,-3));
			$t = $this->vid_types[$ext];
			if ( $t=='v' ){
				$k = substr($img,0,-3).'thm';
				$s = 0;
			}
			else {
				$t = 'i';
				$s = 't';
				$k = $img;
			}
			$fpath = $globs['imagedir'].'/'.$session['userid'].'/'.$path.'/';
			$file = $fpath.$k;

			$xd = read_exif_data_quick($file);

			if( $xd['Width']=='' ){
				$res = '85 x 85';
			}
			else {
				$res = $xd['Width']." x ".$xd['Height'];
			}
			if ( $xd['DateTime']=='' ){
				$cd = filectime($fpath.$img);
			}
			else {
				$cd = "'".$xd['DateTime']."'";
			}
				
			$db = $globs['db'];
			$db->Halt_On_Error = 'No';

			/* look in db to find existing image / directory name */
			$sql = "SELECT * from image WHERE "
				." name='".addslashes($img)."'"
				." AND dir='/".addslashes($path)."'"
				;
			$db->query( $sql );
			if( $db->next_record() ){
				$row = $db->Record;
				$iid = $row['id'];
				
				/* image in database already (image_name,dir) */
#	echo " img_row: $iid, $row[name], $row[dir]<br>";

				$sql = "UPDATE image SET ts=ts"
					.",dir='/".addslashes($path)."'"
					." WHERE id=$iid"
					;
#				$db->query( $sql );
			}
			else {
				/* insert new image into DB */
				$sql = "INSERT INTO image SET"
					." user_id=".$session['userid']
					.",name='".addslashes($img)."'"
					.",dir='/".addslashes($path)."'"
					.",media='$t'"
					.",status='a'"
					.",cd=$cd"
#					.",res='$res'"
					;

				$db->query( $sql );
				$iid = $db->get_last_insert_id('image','id');
			}

			/* add the image / site link */
			$sql = "REPLACE INTO site_image SET"
				." image_id=$iid"
				.",server_id=".$session['srv_id']
				.",res='$res'"
				.",size=".filesize($fpath.$img)
				;
			$db->query( $sql );
			
#			echo "sql: $sql<br>";
			$db->Halt_On_Error = 'Yes';
/*
			return 0;
			$img_src = 'media.php?'.enc64("s=$s&f=$file");
			$tpl_data = array(
					'cnt' => $cnt++,
					'file' => $img,
					'img_path' => $path,
					'img_src' => $img_src,
					);
			$this->t->set_var($tpl_data);
			$this->t->parse('image_d','image_data','true');
*/
			return 1;
		}
	}

