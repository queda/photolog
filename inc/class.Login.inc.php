<?php # $Id: class.Login.inc.php,v 1.7 2003-04-24 22:30:56 paulmcav Exp $

	IncludeObject('Page');
	
	/** Login page
	*
	*/
	class Login extends Page
	{
		function Login()
		{
			global $session, $globs;

			$this->Page('Login');

			$this->t->set_block('body','login_body');

			if (!isset($session['refurl'])
					   	&& $_SERVER['HTTP_REFERER'] != $PHPSELF
						) {
				$session['refurl'] = $_SERVER['HTTP_REFERER'];
			}

# echo "refurl: $session[refurl]<br>";
# echo "<pre>"; print_r( $_REQUEST ); echo "</pre>";

			$msg = '';
			# user requested their password sent to them.
			if ( $_REQUEST['cmd'] == 'Send Password' &&
				($ret = $this->request_pass( $_REQUEST['uid'])) >= 0
				)
			{
				$msg = "A password was sent to the requested email address.";
			}
			else
			{
				# invalid email address
				if ( $ret == -1 )
				{
					$msg = "That email address does not exist!";
				}
			}

			if ( $_REQUEST['cmd'] == 'Logout' )
			{
				$msg = "You have been logged out!";

				unset( $session['loginok'] );
				unset( $session['userid'] );
				unset( $session['userem'] );

				setcookie( 'userid', '',time()-3600,'/' );
			}
			
			# try and log the user in
			if ( $_REQUEST['cmd'] == 'Login' &&
				ValidateUser( $_REQUEST['uid'], $_REQUEST['pass'] )
				)
			{
				if ( $session['refurl']!='' )
					$loc = $session['refurl'];
				else
					$loc = "$PHPSELF?page=Index";
				
				unset( $session['refurl'] );
				Header( "Location: $loc" );
				return; 
#				$var[] = Array(
#					'login_link' => '',
#					'login_body' => '',
#				);
#				$msg = "Thanks for logging in.  Please use the search form below";
			}
			else
		   	{
			}

			
			$var = Array(
				'page_title' => 'Login Form',
				'login_message' => $msg,
				'java_onload' => '',
				'java_script' => '',
			);
			$this->t->set_var( $var );

			$_SESSION['session'] = $session;
		}

		function request_pass( $uid )
		{
			$row = FindUser( $uid );

			if ( !is_array( $row ) )
			{
				return -1;
			}
			else
			{
				# try and send email w/ the password to the found user.
				IncludeObject('email');
				$email = new email('subscribe');

				$var = Array(
					'name' => $row['name'],
					'email' => $row['email'],
					'pass' => $row['passwd'],
					'uid' => $row['id'],
				);
				
				$email->t->set_var( $var );

				$email->send(
					'e-PhotoLog Password Request',
#					'paulmcav',
					$row['email'],
					'paulmcav@queda.net'
					);
			}
			return 0;
		}
	}

