<?php # $Id: class.Info.inc.php,v 1.3 2003-03-27 09:03:38 paulmcav Exp $

	IncludeObject('Page');
	
	/** Info page
	*
	*/
	class Info extends Page
	{
		function Info()
		{
			global $session, $globs;

			$this->Page('Info');
			
#			$this->t->set_block('body','form_part1');
#			$this->t->set_block('body','form_part2');
#			$this->t->set_block('body','form_part3');
			$this->t->set_var('common','{body}');

			// if our image is set, display info for it
			if (isset($session['image_row']) ){
				$row = $session['image_row'];

				// select image filename
				$file = $globs['imagedir'].'/'.$row['user_id'].'/'
					.$row['dir'].'/';
				if ( $row['media']!='v' ) {
					$file .= $row['name'];
				}
				else {
					$file .= substr($row['name'],0,-3).'thm';
				}
				$exif = read_exif_data_quick( $file );
#				ob_start(); print_r($xd);
#			   	$exif_raw = ob_get_contents(); ob_end_clean();
#	echo "<pre>$exif_raw</pre>";

				$exif_buff = '';
				foreach( $exif as $k => $v ){
					if ( $v!='' )
						$exif_buff .= "<tr><td>$k:</td><td>$v</td></tr>\n";
				}
				$row['exif_data'] = $exif_buff;
				
				$this->t->set_var($row);
			}

			$_al= $_REQUEST['aload'];
			if ( isset($session['reload_info']) && !isset($_REQUEST['aload']) )
				$_al = $session['reload_info'];
			
			$var = Array(
				'page_title' => '',
				'java_onload' => '',
				'java_script' => '',
				'al_aut' => ($_al=='1' ? 'checked':''),
				'reload' => ($_al=='1' ? 'automatic':'manual'),
			);
			$this->t->set_var($var);

			$session['reload_info'] = $_al;
		}
		
	}

