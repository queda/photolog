<?php # $Id: class.I_Views.inc.php,v 1.1 2003-06-20 01:22:54 paulmcav Exp $

	IncludeObject('Page');
	
	/** Info page
	*
	*/
	class I_Views extends Page
	{
		function I_Views()
		{
			global $session, $globs;

			$this->Page('I_Views');
			
			$db = $globs['db'];

#			$this->t->set_block('body','form_part1');
#			$this->t->set_block('body','form_part2');
#			$this->t->set_block('body','form_part3');
			$this->t->set_var('common','{body}');

			$r_id = $_REQUEST['id'];

			// if our image is set, display info for it
			if ($r_id>0) {
				$sql = "SELECT *,DATE_FORMAT(iv.ts,'%d%b%Y %h:%i') nicedate"
					." FROM image_view iv"
				   	." LEFT JOIN user u on u.id=iv.user_id"
					." WHERE image_id=".$r_id
					." ORDER BY iv.id"
					;

				$db->query( $sql );

				$_v_data = '';
				while( $db->next_record() ) {
					$row = $db->Record;

					$_v_data .= "<tr>"
						."<td><small>".$row['email']."</small></td>"
						."<td>".$row['res']."</td>"
						."<td><small>".$row['nicedate']."</small></td>"
						."<td>".$row['remote_addr']."</td>"
						."</tr>";
				}
			}
			else 
			if (isset($session['image_row']) ){
				$row = $session['image_row'];

				// select image filename
				$file = $globs['imagedir'].'/'.$row['user_id'].'/'
					.$row['dir'].'/';
				if ( $row['media']!='v' ) {
					$file .= $row['name'];
				}
				else {
					$file .= substr($row['name'],0,-3).'thm';
				}
				$exif = read_exif_data_quick( $file );
#				ob_start(); print_r($xd);
#			   	$exif_raw = ob_get_contents(); ob_end_clean();
#	echo "<pre>$exif_raw</pre>";

				$exif_buff = '';
				foreach( $exif as $k => $v ){
					if ( $v!='' )
						$exif_buff .= "<tr><td>$k:</td><td>$v</td></tr>\n";
				}
				$row['exif_data'] = $exif_buff;
				
				$this->t->set_var($row);
			}

			$_al= $_REQUEST['aload'];
			if ( isset($session['reload_info']) && !isset($_REQUEST['aload']) )
				$_al = $session['reload_info'];
			
			$var = Array(
				'view_data' => $_v_data,
				'page_title' => '',
				'java_onload' => '',
				'java_script' => '',
				'al_aut' => ($_al=='1' ? 'checked':''),
				'reload' => ($_al=='1' ? 'automatic':'manual'),
			);
			$this->t->set_var($var);

			$session['reload_info'] = $_al;
		}
		
	}

