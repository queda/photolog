<?php # $Id: class.Index.inc.php,v 1.14 2003-08-12 15:22:38 paulmcav Exp $

	IncludeObject('Page');
	IncludeObject('stats');
	
	/** Index page
	*
	*/
	class Index extends Page
	{
		function Index()
		{
			global $globs;
			global $session;
			
			$this->Page('Index');
			$this->t->set_block('body','userlist_row','userlist');

			$this->select_rand_image($globs['db']);

			$_new_add_msg = '';
			if ( !$this->get_userlist($globs['db'], $session['userid']) 
					&& $session['userid']
					){
				$_new_add_msg = 'Add <A href="?page=View&st=d&bid=all&uid='
					.$session['userid'].'">your images</a>.';
			}

			$stats = new stats( $globs['db'] );

			$var = Array(
				'page_title'  => '',
				'user_count'  => $stats->User_count,
				'image_count' => $stats->Image_count,
				'book_count'  => $stats->Book_count,
				'add_msg'     => $_new_add_msg,
				'java_onload' => '',
				'java_script' => '',
			);
			$this->t->set_var($var);

			$_SESSION['session'] = $session;
		}

		function get_userlist( $db, $li_uid )
		{
			global $session; 

			$sql = "SELECT COUNT(*) as img_cnt,u.id,u.name,email FROM image i"
				  ." LEFT JOIN user u ON u.id=i.user_id"
				  ." LEFT JOIN site_image si ON i.id=si.image_id"
				  ." WHERE status='a' AND si.server_id="
				  .$session['srv_id']." GROUP BY u.id";

			$db->query( $sql );

			$ret = 0;
			while ( $db->next_record() )
			{
				$row = $db->Record;
				$this->t->set_var( $row );
				$this->t->parse('userlist','userlist_row','true');

				if ( $row['id'] == $li_uid )
					$ret = 1;
			}

			return $ret;
		}

		function select_rand_image( $db )
		{
			global $session, $globs;

			$ssiz  = $globs['imagerez'];

			$sql = "SELECT COUNT(*) cnt,SUM(RAND()),iv.image_id"
				." FROM image_view iv LEFT JOIN site_image si"
				." ON si.image_id=iv.image_id"
				." LEFT JOIN image i ON si.image_id=i.id"
				." WHERE si.server_id=".$session['srv_id']
				." AND i.status='a' AND i.media='i'"
				." GROUP BY iv.image_id HAVING cnt>3  ORDER by 1";

#			echo "sql: $sql<br>";
			$db->query( $sql );
			
			$nr = $db->num_rows();

			if ( $nr ) {
					$nr = rand(1,$nr);
					
					$db->seek($nr);
					$db->next_record();
					$row = $db->Record;
					$views = $row['cnt']." Views.";

#	echo "image_id: $row[image_id]<br>";
					
					$sql = "SELECT *,i.id iid,DATE_FORMAT(cd,'%d%b%y %h:%i') _cd"
						." FROM image i"
						." LEFT JOIN site_image si ON i.id=si.image_id"
						." WHERE i.id=".$row['image_id'];
					$db->query( $sql );
					
					$db->next_record();
					$row = $db->Record;

					$dims = explode('x',$row['res']);
					
					$iscale = @min($ssiz['i']/$dims[0],
									$ssiz['i']/$dims[1]);

					$iw = (int)($dims[0]*$iscale);
					$ih = (int)($dims[1]*$iscale);
					
		#	echo "iw: $iw, ih: $ih<br>";
					
					$img_dim="width=\"$iw\" height=\"$ih\"";
#					$img_src="media.php?".enc64("s=0&f=".$row['user_id']
#						.$row['dir']."/.thumbnails/".$row['name'].".png");
					$img_src="media.php?".enc64("s=t&img=".$row['iid']);

		#	echo "img_src: $img_src<br>";
					
					$bid = substr($row['dir'],0,strrpos($row['dir'],'/'));
					$ds  = strrchr($row['dir'],'/');
					$ds = substr($ds,1);
					$img_url="?".enc64("page=View&uid=".$row['user_id']."&st=d&bid="
						.$bid."&ds=$ds"."&img=".$row['iid']);
					
#			echo "dir: $row[dir]<br>";

					$title = $row['title'];
					if ($title=='') {
						$title = substr($row['name'],0,-4);
					}
					$_cd = $row['_cd'];

					$this->t->set_var($row);
			}
			else {
				$title = '';
				$_cd = '';
				$img_url = '';
				if (rand(0,1)) {
					$img_src = 'img/eye_closed2.gif';
				}
				else {
					$img_src = 'img/eye_open2.gif';
				}
				$img_dim = '';
				$views = '';
			}
			$var = Array(
					"title" => $title,
					"_cd" => $_cd,
					"img_url" => $img_url,
					"img_src" => $img_src,
					"img_dim" => $img_dim,
					"views" => $views,
					);
			$this->t->set_var($var);
		}
	}

