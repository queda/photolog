<?php # $Id: common.inc.php,v 1.11 2003-07-10 20:43:05 paulmcav Exp $

	# common support functions

	function IncludeObject($obj)
	{
		$path = '';
		if ( !($class = substr(strrchr($obj, '/'),1)) )
		{
			$class = $obj;
		}
		else
	   	{
			$path = '/'.substr($obj,0,strrpos($obj,'/'));
		}
		$path .= "/inc";
		
		if ( class_exists($class) )
		{
			return 0;
		}
		include(SERVER_ROOT.$path.'/class.'.$class.'.inc.php');

		return $class;
	}

	function CreateObject($obj, $parms='_F_' )
	{
		$class = IncludeObject($obj);

		$code = '$ret = new '.$class.'(';
		
		for ($i=0;$parms != '_F_' && $i<count($parms);$i++)
		{
			$code .= ($i ? ',':'')."'".$parms[$i]."'";
		}

		$code .= ');';

		eval( $code );
		return $ret;
	}

	function FindUser( $email )
	{
		global $globs;

		$db = $globs['db'];

		if ( $email > 0 )
		{
			$field = "id";
		}
		else 
		{
			$field = "email";
		}
		
		$sql = "SELECT id,email,passwd,name "
			  ."FROM user WHERE $field='$email'";
			  
		if ( $db->query( $sql ) )
		{
			$db->next_record();
			$urow = $db->Record;

			return $urow;
		}
		return false;
	}

	function LocateSite( $server )
	{
		global $session, $globs;
		
		$db = $globs['db'];

		$sql = "SELECT * FROM site WHERE server_name='$server'";
		
		$db->query( $sql );
		// site exist in our database?
		if ( $db->next_record()) {
			$srow = $db->Record;

			$sid = $srow['server_id'];
		}
		// no, then lets log it
		else {
			$sql = "INSERT INTO site SET cd=NOW(),server_name='$server'";
			$db->query( $sql );
			$sid = $db->get_last_insert_id('site','id');
			
			$db->query( "UPDATE site SET server_id=$sid WHERE id=$sid" );
		}
		$session['srv_nm'] = $server;
		$session['srv_id'] = $sid;
	}

	function ValidateUser( $email, $pass )
	{
		global $session;

		# check if logging in as the admin
		if ( $email == 'admin' && $pass = $globs['adminpw'] )
		{
			$session['loginok'] = -1;
			$session['userid'] = -1;
			$session['userem'] = 'admin';
			setcookie( 'userid', -1, 0, "/" );

			return -1;
		}

		$row = FindUser( $email );

		// can't log user in, not found in db
		if ($row==false) {
			$email = '';		

			unset( $session['userid'] );
			unset( $session['userem'] );
			setcookie( 'userid', $user, time()-2678400, "/" );

			return false;
		}

		$name = '';
			
		# validate user w/ pass
		if ( $pass != -1 )
		{
			$user = 0;
			if ( $pass == $row['passwd'] )
			{
				$user = $row['id'];
				
				# only when logging in w/ password
				$session['loginok'] = $user;
			}
		}
		else
		{
			$user = $email;
		}

		if ( $user )
		{
			$parts = explode( '@',$row['email'] );
			$name = $parts[0];
		}
		
		$session['userid'] = $user;
		$session['userem'] = $name;

		setcookie( 'userid', $user, time()+2678400, "/" );

		return $user;
	}

	function gen_http_get( $vars='' )
	{
		parse_str($vars,$rq_data);
		$rq_data = array_merge($_GET,$rq_data);

		while( list($k,$v)=each($rq_data) ) {
			$buff .= ($buff!='' ? '&':'')."$k=$v";
		}
		return $buff;
	}

	function read_exif_data_quick($path) {
		if ( !is_file($path) ) return;

#		$tmpfile = "/var/tmp/exif.dat";
#		$in = fopen($path, "r");  
#		$out = fopen($tmpfile,"w");
#		fwrite( $out, fread( $in, 15000 ) );
#		fclose($out); fclose($in);
#		$exif = read_exif_data($tmpfile);
#		unlink($tmpfile);
		$exif = read_exif_data($path);
		
		if (!isset($exif['DateTime'])) {
			$exif['DateTime'] = date ("Y-m-d H:i:s.", filemtime($path));
		}
	   
		array_shift($exif); array_shift($exif);
		array_shift($exif); 
		unset($exif['SectionsFound']);
		unset($exif['ExifVersion']);
		unset($exif['Thumbnail']);
		unset($exif['THUMBNAIL']);
		unset($exif['COMPUTED']['html']);
		$exif = array_merge($exif['COMPUTED'],$exif);
		unset($exif['COMPUTED']);
		unset($exif['ThumbnailSize']);
		unset($exif['Orientation']);

		foreach( $exif as $k => $v ){
			if ( ord($v)<32 )
				unset($exif[$k] );
		}
	  return $exif;
	} 

