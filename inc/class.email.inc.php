<?php # $Id: class.email.inc.php,v 1.1 2002-10-29 22:16:27 paulmcav Exp $

	IncludeObject('Template');

	/** generic class to handle templated email messages.
	*
	*/
	class email
	{
		var $t;
		var $t_name;
		
		function email( $name, $policy='remove' )
		{
			$this->t_name = $name;
			$this->t = new Template( 'email', $policy );

			$this->t->set_file( $name, $name.'.txt' );
		}

		#function send($subj, $to, $from, $headers[] = '')
		function send($subj, $to, $from)
		{
			global $session;

			$var = Array(
				'Subject' => $subj,
				'To' => $to,
				'From' => $from,
			);
			
			$this->t->set_var( $var );
			$this->t->set_var( $headers );

			$this->t->parse('out',$this->t_name);

			$msg = $this->t->get('out');

			$Headers = "From: $from\r\n";
			while( is_array($headers) && list($k,$v) = each($headers) )
			{
				$Headers .= "$k: $v\r\n";
			}
			
			mail( $to, $subj, $msg, $Headers, "-f$from" );
		}
	}
