<?php # $Id: class.Book.inc.php,v 1.24 2003-08-11 17:41:51 paulmcav Exp $

	IncludeObject('Page');

   	
	# substring_index(dir,'/',1);

	/** View book page
	*
	*/
	class Book extends Page
	{ 
		var $db;

		# constructor
		function Book()
		{
			global $session, $globs;

			$uid = $globs['uid'];
			$bid = $globs['bid'];
			$st  = $globs['st'];

			if ( $_REQUEST['bid'] < 1 ) $_REQUEST['bid'] = 0;

			$this->Page('Book',$globs['tpl_vars']);
			
			$this->t->set_block('body','book_row','book_r');

			$this->db = $globs['db'];

			$this->t->set_var( "def_book_view",'' );

			// check to see if we want to support forms
			if ( $_REQUEST['uid'] == $session['userid'] ) {
				$session['form_data']['_uid'] = $session['userid'];
			}
			else {
				unset( $session['form_data'] );
			}
			
#	$session['srv_id'] = 3;

			// handle form actions
			if (isset($session['form_data']['_uid'])) {
				if ( isset($_REQUEST['fc_ab']) ){
					$_view_message = $this->form_bk_add( $session['userid'],
							$_REQUEST['newbook']);
				}
				$f_mode='';
				if ( isset($_REQUEST['fc_upd']) ){
					$f_mode = 'u';
				}
				if ( isset($_REQUEST['fc_ab']) ){
					$f_mode = 'a';
				}
				if ( isset($_REQUEST['fc_cb']) ){
					$f_mode = 'c';
				}
				if ( isset($_REQUEST['fc_db']) ){
					$f_mode = 'd';
				}
				if ( $f_mode!='' )
					$_view_message = $this->form_update($f_mode);
			}
			
			if ( !isset($session['form_data']['_uid']) ) {
				$_form = "";
				$this->t->set_block('body','usr_bk_ehdr','u_bk_h');
				$this->t->set_block('book_row','usr_bk_erow','u_bk_er');
				$this->t->set_block('body','form_action','form_a');
			}

			if ( isset($_REQUEST['tree']) && isset($session['bk_tree']) ) {
					$t = $_REQUEST['tree'];
					unset($_GET['tree']);
					unset($_REQUEST['tree']);
					$session['bk_tree'][$t] *= -1;
			}
				
			#  Get book info
			$cnt = $this->find_books($_REQUEST['uid'],$_REQUEST['st']);
			
			if ( isset($session['form_data']['_uid']) ) {
				$_form = "<FORM name=\"bookList\" action=\"?"
					.enc64(gen_http_get())
					."\" method=\"POST\">";
				$_f_update = '<INPUT type="submit" name="fc_upd" value="Update">';

				$session['form_data']['cnt'] = $cnt;
			}
			else {
			}

			if ( $_REQUEST['bid']>0 ){
				$this->book_detail( $_REQUEST['bid'] );
			}
			else {
				$this->t->set_block('body','book_detail','bk_dtl');
				$this->t->set_block('body','form_detail','frm_dtl');
				
#				$var = array( 'bk_dtl' => '', 'frm_dtl' => '' );
#				$this->t->set_var($var);
			}
			
			# fill out some more form variables
			$var = Array(
				'page_title' => 'Usr#'.$_REQUEST['uid'].' Books',
				'view_message' => $_view_message,
				'java_onload' => '',
				'java_script' => '',
				'book_cnt' => $session['form_data']['cnt'],
				'f_update' => $_f_update,
				'form' => $_form,
			);
			$this->t->set_var($var);

			unset( $session['book_data'] );
			unset( $session['refurl'] );
			
#	$session['srv_id'] = 2;
#			$_SESSION['session'] = $session;
		}

		/**
		 *
		*/
		function disp_books( $book_list, $pid, $lvl )
		{
			global $session, $bk_cnt;

			if ( $lvl == 0 ) $bk_cnt = 0;
			
			for ($i=0;$i<sizeof($book_list[1]);$i++) {

				// skip row if not set || ! selected pid
				if ( !isset($book_list[1][$i]) 
					 || $book_list[2][$book_list[1][$i]]['p_id'] != $pid ) {
					continue;
				}
				$row = $book_list[2][$book_list[1][$i]];
					
				$this->t->set_var( $row );
				
				$u_id = $_REQUEST['uid'];
				
				$url = "uid=$u_id&bid=".$row['id'];
				$bkvw_link = enc64("page=View&$url");
				
				if ( $row['p_id'] && $row['p_id'] != $row['id'] )
					$bknm_timg = "<IMG border=0 src=\"img/1.gif\" height=5"
						." width=\"".(($lvl-1)*9)."\">"
						."<IMG src=\"img/L.gif\" border=0>";
				else
					$bknm_timg = '';

				$nurl = enc64("page=Book&$url");
				$bknm_link = "<A href=\"?$nurl\">".$row['name']."</A>";
				
				$_bk_row = 'class="book_bg';
				if ($session['form_data']['_uid']) {
					if ( strstr($row['status'],'c') )
					   	$_bk_row = 'class="book_close';
					if ( !strstr($row['status'],'a') )
						$bknm_link = "<strike>$bknm_link</strike>";
				}
				$_bk_row .=($bk_cnt%2).'"';
				
				// form support has been enabled
#				if (1) {
					$_radio_p = "<INPUT type=\"radio\" name=\"f_book_p\""
							." value=\"".$row['id']."\">";
					$_check_c = "<INPUT type=\"checkbox\" name=\"f_book_c[$i]\""
							." value=\"".$row['id']."\">";
					$_check_acd = "<INPUT type=\"checkbox\""
						." name=\"f_book_acd[$i]\" value=\"".$row['id']."\">";
#				}
				
				$c_num = $book_list[0][$row['id']];
				$tree_link = '<IMG src="img/1.gif" height=1 width=14 border=0>';
				$_view_img_siz = 'width="40" height="23"';
				
				if ( $c_num<1 ) {
				   	$c_num = '~';
					if ( $row['img_cnt']<1 )
						$_view_img_siz = 'width="0" height="0"';
				}
				else {
					$tree_link = "<A href=\"?"
						.enc64(gen_http_get("tree=".$row['id']))."\">"
						.'<IMG height=14 width=14 border=0 src="img/';
					if ( $session['bk_tree'][$row['id']] >0 )
						$tree_link .= "arrow-sd_g.gif";
					else
						$tree_link .= "arrow-sr_g.gif";
					$tree_link .= "\"></a>";
				}
					
				$var = array(
						"br_d_ops" => '',
						"bkvw_lnk" => $bkvw_link,
						"tree_lnk" => $tree_link,
						"bknm_timg" => $bknm_timg,
						"bknm_lnk" => $bknm_link,
						"radio_p" => $_radio_p,
						"check_c" => $_check_c,
						"check_acd" => $_check_acd,
						"c_num" => $c_num,
						"bk_row" => $_bk_row,
						"cnt" => $i,
						"view_img_siz" => $_view_img_siz,
				);
				$this->t->set_var( $var );
				$this->t->parse('book_r','book_row','true');
					
				$bk_cnt++;
				// has some children with this node, descend
				if ( $book_list[0][$row['id']] >0
						&& $session['bk_tree'][$row['id']]>0
				   ) {
					$this->disp_books( $book_list, $row[id], $lvl+1 );
				}
			}
		}
		
		/**
		 *
		*/
		function find_books($u_id,$stat) {
			global $session;
			
			// looking at directory mode
			$url = enc64("page=View&st=d&uid=$u_id&bid=all");
			$_all_img_link = "<A href=\"?$url\">Entire Image Collection</a>";
			
			$b_status = "b.status "
				.($session['form_data']['_uid']?"IS NOT NULL":"LIKE '%a%'");
			
			// get a list of book children
			$sql = "SELECT p_id,COUNT(*) FROM book b"
#				." LEFT JOIN book_image bi on b.id=bi.book_id"
				." WHERE p_id>0 AND "
				.$b_status." GROUP by p_id";
#	echo "S: $sql<br>";			
			$this->db->query( $sql );
			while ( $this->db->next_record() ) {
				$row = $this->db->Record;
				$book_list[0][$row[0]] = $row[1];

#	echo "bl0: $row[1]<br>";
				if ( !isset($session['bk_tree'][$row[0]]) )
					$session['bk_tree'][$row[0]] = -1 ;
			}

			// scan database for starting point.
			$sql = "SELECT b.*,"
				  ."SUM(IF(bi.image_id is not null,1,0)) AS img_cnt"
				  .",DATE_FORMAT(b.cd,'%d%b%y') AS _cd,si.server_id"
				  ." FROM book b"
				  ." LEFT JOIN book_image bi ON b.id=bi.book_id"
				  ." LEFT JOIN site_image si ON bi.image_id=si.image_id"
				  ." WHERE $b_status AND b.user_id=$u_id"
#				  ." AND si.server_id=".$session['srv_id']
				  ." GROUP BY b.id"
				  ." HAVING (si.server_id IS NULL OR si.server_id=".$session['srv_id'].")"
				  ." ORDER BY b.cd";
				
#	echo "Q: $sql<br>";
			$this->db->query( $sql );
			$cnt = 0;
			
			while ( $this->db->next_record() ) {
				$bkr = $this->db->Record;
					
				if ($bkr['img_cnt']<1)
					$bkr['_cd'] = '';
				$book_list[1][$cnt] = $bkr['id'];
				$book_list[2][$bkr['id']] = $bkr;
				$cnt++;
			}
#	echo "<pre>"; print_r( $book_list ); echo "</pre>";
			$this->disp_books( $book_list, 0,0 );

			$var = array( 
				"all_img_link" => $_all_img_link,
				);
			$this->t->set_var( $var );

			return $cnt;
		}

		/**
		 *
		*/
		function book_detail( $b_id )
		{
			global $session;

			$sql = "SELECT b.*,si.*,i.title AS i_title"
				  .",SUM(IF(bi.image_id is not null,1,0)) AS img_cnt"
				  .",DATE_FORMAT(b.cd,'%d%b%y') AS _cd"
				  ." FROM book b"
				  ." LEFT JOIN book_image bi ON b.id=bi.book_id"
				  ." LEFT JOIN site_image si ON b.image_id=si.image_id"
				  ." LEFT JOIN image i ON si.image_id=i.id"
				  ." WHERE b.id=$b_id"
				  ." AND si.server_id=".$session['srv_id']
				  ." GROUP BY b.id";

			$this->db->query( $sql );
			$this->db->next_record();

			$row = $this->db->Record;
			$this->t->set_var( $row );

#	echo "sql: $sql<br>";
#	echo "<pre>"; print_r($row); echo "</pre>";
			// show thumbnail image
			if ( $row['image_id']>0 ) {
				
				$dims = explode('x',$row['res']);
				$iscale = @min(85/$dims[0],85/$dims[1]);
				$iw = (int)($dims[0]*$iscale);
				$ih = (int)($dims[1]*$iscale);

				$_book_image = '<IMG border="1" width="'.$iw.' height="'.$ih
					.'" title="'.$row['i_title'].'" src="media.php?'
					.enc64('s=t&img='.$row['image_id'])
					.'">';
			}
			else 
				$_book_image = '';
			
			$u_id = $_REQUEST['uid'];
			$url = "uid=$u_id&bid=".$row['id'];
			$bkvw_link = enc64("page=View&$url");
			
			if ( $session['form_data']['_uid'] ) {
				$_form_data .= "";
				$_name = "<INPUT type=text name=\"d_bk[0]\" size=\"50\""
					." value=\"".$row['name']."\">";
				$_notes = "<TEXTAREA wrap=soft cols=44 rows=4 name=\"d_bk[1]\">"
					.$row['notes']."</TEXTAREA>";
				$_date = "<INPUT type=text name=\"d_bk[2]\" size=\"10\""
					." value=\"".$row['cd']."\">";

				$_image = '~ Book Image:<SELECT name="d_bk[3]">'
					.$this->images_get_list($b_id,$row['image_id'])
					.'</SELECT';
				
				$_status = $row['status'];
			}
			else {
				$_name = $row['name'];
				$_notes = $row['notes'];
				$_date = $row['_cd'];
				$_image = '';
				$_status = '';
			}
				
			$var = array( 
				"_name" => $_name,
				"_notes" => $_notes,
				"_date" => $_date,
				"_status" => $_status,
				"book_image" => $_book_image,
				"_image" => $_image,
				"bkvw_lnk" => $bkvw_link,
				"form_data" => $_form_data,
				);
			$this->t->set_var( $var );
		}

		/**
		 *
		*/
		function form_bk_add( $u_id, $bk_name )
		{
			if ( $bk_name=='' )
				return "";

			$this->db->Halt_On_Error = 'no';
			$sql = "INSERT INTO book (user_id,name,cd)"
				." VALUES($u_id,'$bk_name',NOW())";

			$this->db->query( $sql );
			$this->db->Halt_On_Error = 'yes';
			
			return "Book '$bk_name' added.";
		}

		/**
		 *
		*/
		function form_update($mode)
		{
			global $session;

#			echo "<pre>"; print_r( $_REQUEST ); echo "</pre>";
			$cnt = $session['form_data']['cnt'];

			$msg = '';
			$m_pc = $m_acc = $m_cls = $m_del = 0;
			
			// update book detail info
			$bk_d = $_REQUEST['d_bk'];
			if ( isset($bk_d) ){
				$sql = "UPDATE book SET name='".addslashes($bk_d[0])
					."',notes='".addslashes($bk_d[1])
					."',cd='".$bk_d[2]
					."',image_id=".$bk_d[3]
					." WHERE id=".$_REQUEST['bid'];
				$this->db->query( $sql );

				$msg .= "Book detail info updated.<br>";
			}

			for ($i=0;$i<=$cnt;$i++) {
				if ( $mode=='u' ) {
					$_p = $_REQUEST['f_book_p'];
					$_c = $_REQUEST['f_book_c'][$i];
					
					if ($_p>0 && $_c>0) {
						// reset child row.
						if ( $_p == $_c ) $p = 0;
						else $p = $_p;
							
						// update child
						$sql = "UPDATE book SET p_id=$p WHERE id=$_c";
						$this->db->query( $sql );
						$m_pc++;
					}
				}

				// update access / close / delete
				$acd = $_REQUEST['f_book_acd'][$i];
				if ( $acd<1 ) continue;
				if ( $mode=='a' && $_REQUEST) {
					$sql = "UPDATE book SET status=IF(FIND_IN_SET('a',status)"
						.",REPLACE(status,'a',''),CONCAT(status,',a'))"
						." WHERE id=$acd";
					$m_acc++;
				}
				if ( $mode=='c' ) {
					$sql = "UPDATE book SET status=IF(FIND_IN_SET('c',status)"
						.",REPLACE(status,'c',''),CONCAT(status,',c'))"
						." WHERE id=$acd";
					$m_cls++;
				}
				if ( $mode=='d' ) {
					$sql = "DELETE FROM book WHERE id=$acd";
					$this->db->query( $sql );

					$sql = "DELETE FROM book_image WHERE book_id=$acd";
					$m_del++;
				}
				if ( $sql!='' ) $this->db->query( $sql );
			}
			if ( $m_pc>0 )
				$msg .= "Updated $m_pc Parent-Child settings.<br>";
			if ( $m_cls>0 )
				$msg .= "Closed $m_cls books.<br>";
			if ( $m_del>0 )
				$msg .= "Deleted $m_del books.<br>";
			
			return $msg;
		}

		/**
		 *
		*/
		function images_get_list($b_id,$i_id)
		{
			global $session;

			$sql = "SELECT i.* from book_image bi LEFT JOIN image i"
				." ON bi.image_id=i.id WHERE book_id=$b_id"
				." AND i.status='a' AND i.media!='v'"
				." ORDER BY cd";

			$this->db->query( $sql );
			
			$op_list = '<OPTION value="0">No Image</OPTION>\n';
			while ( $this->db->next_record() ) {
				$row = $this->db->Record;
				$op_list .= '<OPTION value="'.$row['id'].'"'
					.($i_id==$row['id'] ? " SELECTED" :'').'>'
					.$row['name']."</OPTION>\n";
			}
			return $op_list;
		}

	}

