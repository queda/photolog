<?php # $Id: class.Signup.inc.php,v 1.7 2002-11-13 01:26:54 paulmcav Exp $

	IncludeObject('Page');
	
	/** Signup page
	*
	*/
	class Signup extends Page
	{
		function Signup()
		{
			global $session;

			$this->Page('Signup');
			
			$this->t->set_block('body','form_part1');
			$this->t->set_block('body','form_part2');
			$this->t->set_block('body','form_part3');

			$gemail = $_REQUEST['email'];
			$msg = '';
			$p = 0;

			if (!isset($session['refurl'])
					   	&& $_SERVER['HTTP_REFERER'] != $PHPSELF
						) {
				$session['refurl'] = $_SERVER['HTTP_REFERER'];
			}

			if ( $_REQUEST['cmd'] == 'Submit' )
			{
				# 1st pass of signup
				if ( $_REQUEST['p'] == 1 )
				{
					if ( !$this->valid_email($gemail) )
					{
						$p = 0;
						$msg = "Sorry, <b>'".$gemail."'</b> is an invalid email address.<p>";
					}
					else
				   	if ( $this->find_user($gemail) )
					{
						$msg = "Sorry, <b>'".$gemail."'".'</b> already exists.<br>  Please submit a new email address, or <a href="?page=Login">request a password</a> be sent to you.<p>';
						$p = 0;
					}
					# part 1 of signup appears to be okay
					else
					{
						$session['email'] = $gemail;
						$p = 1;
					}
				}
				# 2nd pass of signup
				else
				# .. email address not same
				if ( $session['email'] != $gemail )
				{
					$msg  = "Your submitted email address do not match!  Please try again.<br>";

					$gemail = $session['email'];
					$p = 1;
				}
				else
				# .. password problem
				if ( $_REQUEST['pass'] != $_REQUEST['pass1']
			   		|| $_REQUEST['pass'] == '' )
				{
					$msg  = "The was a problem with your submitted password(s).  Please try again.<br>";

					$p = 1;
				}
				# 2nd pass looks okay for signup
				else
				{
					
					$msg = 'Your signup form has been submitted.  Please <a href="?page=Login">Login Now</a>.';
					
					$p = 2;
					$this->t->set_var('form_part1','');
					$this->t->set_var('form_part2','');
					$this->t->set_var('form_part3','');

					$uid = $this->register_user();
					if ( $uid != 0 )
					{
						$this->send_message( $uid );
						ValidateUser( $uid, $_REQUEST['pass'] );
						
						if ( $session['refurl']!='' )
							$loc = $session['refurl'];
						else
							$loc = "$PHPSELF?page=Index";
						
						unset( $_SESSION['session']['refurl'] );
						unset( $_SESSION['session']['email'] );

						Header( "Location: $loc" );
						return; 
					}
					# some problem registering user ;(
					else
					{
						$msg = 'There was some type of a problem.  Please contact an <a href="mailto:www@biddercheck.net">admin</a>.';
					}
				}
			}
			# generic stuff for 1st pass
			if ( $p == 0 )
			{
				$msg .= "Step 1: Enter your email address:";
				$this->t->set_var('form_part2','');
			}
			else
			# generic stuff for 2nd pass
			if ( $p == 1 )
			{
				$msg .= "Step 2: Enter your information:";
				$this->t->set_var('form_part1','');
			}

			$var = Array(
				'page_title' => 'Signup Form',
				'step_message' => $msg,
				'java_onload' => '',
				'java_script' => '',
				'email' => $gemail,
				'name' => $_REQUEST['name'],
				'address' => $_REQUEST['address'],
				'city' => $_REQUEST['city'],
				'state' => $_REQUEST['state'],
				'zip' => $_REQUEST['zip'],
				'phone' => $_REQUEST['phone'],
			);
			$this->t->set_var($var);

			$_SESSION['session'] = $session;
		}

		function find_user( $email )
		{
			global $globs;
			$db = $globs['db'];
			
			$sql = "SELECT id FROM user WHERE email='$email'";
			$r = $db->query( $sql );
			return $db->next_record();
		}

		function valid_email( $email )
		{
		   if (eregi("^[^@[:space:]]+@([[:alnum:]\-]+\.)+[[:alnum:]][[:alnum:]][[:alnum:]]?$", $email) )
		   {
			   return 1;
		   }
		   return 0;
		}

		function register_user()
		{
			global $globs;
			$db = $globs['db'];

			$sql = "INSERT INTO user "
				  ."(email,passwd,name,address,city,state,zip,phone,cd) "
				  ."VALUES('".$_REQUEST['email']."'"
				  .",'".$_REQUEST['pass']."'"
				  .",'".$_REQUEST['name']."'"
				  .",'".$_REQUEST['address']."'"
				  .",'".$_REQUEST['city']."'"
				  .",'".$_REQUEST['state']."'"
				  .",'".$_REQUEST['zip']."'"
				  .",'".$_REQUEST['phone']."'"
				  .",now()"
				  .")";

			$db->query( $sql );
			
			$uid = $db->get_last_insert_id('user','id');

			return $uid;
		}

		function send_message( $uid )
		{
			global $session;

			IncludeObject('email');
			$email = new email('subscribe');

			$var = Array(
					'name' => $_REQUEST['name'],
					'email' => $session['email'],
					'pass' => $_REQUEST['pass'],
					'uid' => $uid,
				);
				
			$email->t->set_var( $var );

			$email->send(
				'e-PhotoLog Signup Form',
#				"\"".$_REQUEST['name']."\" <".$session['email'].">",
				$session['email'],
				'paulmcav@queda.net'
				);
		}
	}

