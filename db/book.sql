-- MySQL dump 8.23
--
-- Host: veuropa    Database: photolog
---------------------------------------------------------
-- Server version	3.23.58

--
-- Table structure for table `book`
--

CREATE TABLE book (
  id int(11) unsigned NOT NULL auto_increment,
  p_id int(11) unsigned NOT NULL default '0',
  user_id int(11) unsigned NOT NULL default '0',
  name varchar(255) default NULL,
  notes text,
  status set('a','c') default 'a',
  priv tinyint(3) default '127',
  datedesc varchar(32) NOT NULL default '',
  image_id int(11) NOT NULL default '0',
  cd date default NULL,
  ts timestamp(14) NOT NULL,
  PRIMARY KEY  (id),
  UNIQUE KEY un (user_id,name),
  FULLTEXT KEY nn (name,notes)
) TYPE=MyISAM;

