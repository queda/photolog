-- MySQL dump 8.23
--
-- Host: veuropa    Database: photolog
---------------------------------------------------------
-- Server version	3.23.58

--
-- Table structure for table `book_image`
--

CREATE TABLE book_image (
  id int(11) unsigned NOT NULL auto_increment,
  book_id int(11) unsigned NOT NULL default '0',
  image_id int(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (id),
  UNIQUE KEY bookimage (book_id,image_id)
) TYPE=MyISAM;

