-- MySQL dump 8.23
--
-- Host: localhost    Database: photolog
---------------------------------------------------------
-- Server version	3.23.58

--
-- Table structure for table `user`
--

CREATE TABLE user (
  id int(11) unsigned NOT NULL auto_increment,
  email varchar(50) NOT NULL default '',
  passwd varchar(20) NOT NULL default '',
  name varchar(50) NOT NULL default '',
  address varchar(81) NOT NULL default '',
  city varchar(41) NOT NULL default '',
  state char(3) NOT NULL default '',
  zip varchar(10) NOT NULL default '',
  phone varchar(12) NOT NULL default '',
  country varchar(41) NOT NULL default '',
  visits int(11) default '0',
  priv tinyint(3) default '0',
  img_size tinyint(3) default NULL,
  cd datetime default NULL,
  ts timestamp(14) NOT NULL,
  PRIMARY KEY  (id),
  UNIQUE KEY email (email)
) TYPE=MyISAM;

--
-- Dumping data for table `user`
--


INSERT INTO user VALUES (1,'paulmcav@queda.net','mysecret','Paul McAvoy','','San Francisco','CA','','','',0,0,NULL,'2002-10-25 12:07:43',20021025120743);
INSERT INTO user VALUES (2,'filmyer@csmedia.org','1tfvideo','tom','','san francisco','ca','','','',0,0,NULL,'2002-11-04 16:21:47',20021104162147);
INSERT INTO user VALUES (3,'arjun.s.mcavoy@us.pwcglobal.com','virgil08','Arjun McAvoy','','San Francisco','CA','','','',0,0,NULL,'2002-11-04 16:52:30',20021104165230);
INSERT INTO user VALUES (4,'kent@thisway.net','tnek','Kent Howard','','San Francisco','CA','','','',0,0,NULL,'2002-11-04 23:07:57',20021104230757);
INSERT INTO user VALUES (5,'prem@pjvintage.com','1950','Prem','','Santa Barbara','Ca','','','',0,0,NULL,'2002-11-05 18:05:33',20021105180533);
INSERT INTO user VALUES (6,'anttabora@hotmail.com','ajt12326','Antoinette Tabora','','San Francisco','CA','','','',0,0,NULL,'2002-11-06 13:30:23',20021106133023);
INSERT INTO user VALUES (7,'lagreca@hotmail.com','blue-tip','Joe','','San Francisco','CA','','','',0,0,NULL,'2002-11-06 14:17:44',20021106141744);
INSERT INTO user VALUES (8,'susan@sutodesign.com','booty','susan tobiason','3460 18th Street','San Francisco','CA','94110','415-626-2056','',0,0,NULL,'2002-11-06 22:40:08',20021106224008);
INSERT INTO user VALUES (9,'ay1201@hotmail.com','mantaray','Ayumi Yoshida','333 Market St.','SF','CA','94105','415-498-8032','',0,0,NULL,'2002-11-07 19:58:43',20021107195843);
INSERT INTO user VALUES (10,'mikeand@verizon.net','pictures','Michael Anderson','123 Main Street','Anywhere','CA','USA','510-882-3344','',0,0,NULL,'2002-11-08 12:31:53',20021108123153);
INSERT INTO user VALUES (11,'julie.j.yeh@us.pwcglobal.com','ucla1234','Julie','2537 Regent St. #306','Berkeley','CA','94704','510-548-6551','',0,0,NULL,'2002-11-11 13:20:33',20021111132033);
INSERT INTO user VALUES (12,'elassahn@ndnu.edu','earthbound','eL','2225 23rd st. #310','San Francisco','CA','94107','648-7884','',0,0,NULL,'2002-11-14 17:49:10',20021114174910);
INSERT INTO user VALUES (13,'wlee@sandisk.com','sandisk','Wes Lee','140 Caspian Ct','Sunnyvale','CA','94089','4085480105','',0,0,NULL,'2002-11-21 14:21:36',20021121142136);
INSERT INTO user VALUES (14,'robert@kitson.com','password','Robert Kitson','2615 Hacienda Ct.','Santa Barbara','CA','93105','8054555555','',0,0,NULL,'2002-11-29 22:27:54',20021129222754);
INSERT INTO user VALUES (15,'lrc13@humboldt.edu','petra','leyla','2148 western ave','arcata','ca','95521','822-7812','',0,0,NULL,'2002-12-05 12:04:21',20021205120421);
INSERT INTO user VALUES (16,'emcwayne@nwetc.org','schmegma','Brother E','6558 16th Ave. SW','Seattle','WA','98106','206-762-1976','',0,0,NULL,'2003-01-12 19:27:42',20030112192742);
INSERT INTO user VALUES (17,'jgvchinita@hotmail.com','china1','Jenny','dfasdfsd','San Francisco','ca','94110','4155588602','',0,0,NULL,'2003-01-25 21:36:33',20030125213633);
INSERT INTO user VALUES (18,'brendan@brendanconrad.com','photo','Brendan Conrad','27157 Manor Circle','Valencia','CA','91354','9999999999','',0,0,NULL,'2003-01-29 13:22:36',20030129132236);
INSERT INTO user VALUES (19,'mlynch5ie@yahoo.co.uk','MOBILE12','Martin Lynch','Union Street','SF','CA','94133','415 9220494','',0,0,NULL,'2003-02-11 08:49:38',20030211084938);
INSERT INTO user VALUES (20,'x@y.com','erush','mine','here','San Francisco','Ca','94118','','',0,0,NULL,'2003-02-11 08:53:25',20030211085325);
INSERT INTO user VALUES (21,'rogerssuzanne@hotmail.com','piece4hut','suzanne','','','','','','',0,0,NULL,'2003-02-11 08:57:06',20030211085706);
INSERT INTO user VALUES (22,'janet_s_lee@hotmail.com','jsl143ch','Janet','','','','','','',0,0,NULL,'2003-02-11 09:12:11',20030211091211);
INSERT INTO user VALUES (23,'anitankapoor@yahoo.com','economics','Anita','','','','','','',0,0,NULL,'2003-02-11 09:46:14',20030211094614);
INSERT INTO user VALUES (24,'wendykao@yahoo.com','richard','Wendy','You wanna know?','San Francisco','CA','94110','626-222-5332','',0,0,NULL,'2003-03-15 14:58:32',20030315145832);
INSERT INTO user VALUES (25,'farshid@sandisk.com','sudi','','','','','','','',0,0,NULL,'2003-04-04 21:15:50',20030404211550);
INSERT INTO user VALUES (26,'garret@yamaguchiusa.com','pebbles11','Garret Yamaguchi','','','','','909.437.4919','',0,0,NULL,'2003-04-09 23:42:11',20030409234211);
INSERT INTO user VALUES (27,'kriswidger@mac.com','wilco','kris','3555 cesar chavez','San Francisco','CA','94110','4156416608','',0,0,NULL,'2003-04-14 12:40:49',20030414124049);
INSERT INTO user VALUES (28,'bryan@netmeme.org','hipaul','Bryan Field-Elliot','on file','on file','on','on file','(303) 499-25','',0,0,NULL,'2003-05-12 12:49:00',20030512124900);
INSERT INTO user VALUES (29,'njav@ieee.org','iceland','judy villa','924 anacapa, 2H','Santa Barbara','ca','93101','805-682-9885','',0,0,NULL,'2003-05-12 12:51:52',20030512125152);
INSERT INTO user VALUES (30,'mitchee_chung@hotmail.com','pictures','Mitchee Chung','','','','','','',0,0,NULL,'2003-05-13 09:48:48',20030513094848);
INSERT INTO user VALUES (31,'rmcfaden@itsmanagement.net','123123','ryan mc','somewhere in Australia','probably Sydney','NSW','2022','1111111','',0,0,NULL,'2003-05-15 04:47:56',20030515044756);
INSERT INTO user VALUES (32,'crode@g-p.co.uk','charlie','Charlotte Rode','19 arkwright road','Hampstead','Lon','NW3 6AA','07786085130','',0,0,NULL,'2003-05-19 05:03:01',20030519050301);
INSERT INTO user VALUES (33,'quedanetSpam@xpollen8.com','xing','','','','','','','',0,0,NULL,'2003-05-21 13:19:11',20030521131911);
INSERT INTO user VALUES (34,'carolin@thurm.net','jthurm','','','','','','','',0,0,NULL,'2003-06-16 11:48:52',20030616114852);
INSERT INTO user VALUES (35,'marc@blasedesign.com','mb1547','marc','street','truckee','ca','9612','5305879225','',0,0,NULL,'2003-06-19 10:10:45',20030619101045);
INSERT INTO user VALUES (36,'lisaschembri@hotmail.com','ralfie','Lisa Schembri','150 California Street','San Francisco','CA','94111','','',0,0,NULL,'2003-04-28 09:39:28',20030630161812);
INSERT INTO user VALUES (37,'mchung@citco.com','pictures','Mitchee','Chung','','','','','',0,0,NULL,'2003-04-28 09:54:22',20030630161812);
INSERT INTO user VALUES (38,'vmandradejr@hotmail.com','red974','Victor M Andrade Jr','','','','','','',0,0,NULL,'2003-05-10 02:23:45',20030630161812);
INSERT INTO user VALUES (39,'mheimer@msn.com','golfnuts','Mark Rosenheimer','1067 Valencia St. #12','San Francisco','CA','94110','415-826-8974','',0,0,NULL,'2003-05-30 14:11:23',20030630161812);
INSERT INTO user VALUES (40,'mclagreca@hotmail.com','Betty$12','Megan La Greca','7292 Birchcreek Rd','San Diego','CA','92119','','',0,0,NULL,'2003-05-31 23:22:40',20030630161812);
INSERT INTO user VALUES (41,'mail@d-a-n.com','greggy','Danny Schambach','81 Langton St.','San Francisco','CA','94109','415-863-1555','',0,0,NULL,'2003-06-24 06:24:07',20030630161812);

