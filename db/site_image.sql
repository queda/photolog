-- MySQL dump 8.23
--
-- Host: veuropa    Database: photolog
---------------------------------------------------------
-- Server version	3.23.58

--
-- Table structure for table `site_image`
--

CREATE TABLE site_image (
  id int(11) unsigned NOT NULL auto_increment,
  image_id int(11) unsigned NOT NULL default '0',
  server_id int(11) unsigned NOT NULL default '0',
  size int(11) default NULL,
  res varchar(14) default NULL,
  xtra blob,
  ts timestamp(14) NOT NULL,
  PRIMARY KEY  (id),
  UNIQUE KEY image_site (image_id,server_id)
) TYPE=MyISAM;

