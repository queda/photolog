-- MySQL dump 8.23
--
-- Host: veuropa    Database: photolog
---------------------------------------------------------
-- Server version	3.23.58

--
-- Table structure for table `site`
--

CREATE TABLE site (
  id int(11) unsigned NOT NULL auto_increment,
  server_id int(11) unsigned NOT NULL default '0',
  server_name varchar(255) default NULL,
  notes text,
  status set('a','c') default 'a',
  priv tinyint(3) default '127',
  cd date default NULL,
  ts timestamp(14) NOT NULL,
  PRIMARY KEY  (id),
  KEY server_id (server_id)
) TYPE=MyISAM;

