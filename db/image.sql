-- MySQL dump 8.23
--
-- Host: veuropa    Database: photolog
---------------------------------------------------------
-- Server version	3.23.58

--
-- Table structure for table `image`
--

CREATE TABLE image (
  id int(11) unsigned NOT NULL auto_increment,
  user_id int(11) unsigned NOT NULL default '0',
  name varchar(32) NOT NULL default '',
  dir varchar(255) NOT NULL default '',
  status set('a') NOT NULL default '',
  media set('i','p','v') NOT NULL default '',
  priv tinyint(3) NOT NULL default '127',
  title varchar(128) default NULL,
  notes text,
  kwds text,
  views int(11) unsigned default '0',
  xtra set('T') default NULL,
  cd datetime default NULL,
  ts timestamp(14) NOT NULL,
  PRIMARY KEY  (id),
  UNIQUE KEY namedir (name,dir),
  FULLTEXT KEY namedirnotes (name,dir,notes)
) TYPE=MyISAM;

