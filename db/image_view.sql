-- MySQL dump 8.23
--
-- Host: veuropa    Database: photolog
---------------------------------------------------------
-- Server version	3.23.58

--
-- Table structure for table `image_view`
--

CREATE TABLE image_view (
  id int(11) unsigned NOT NULL auto_increment,
  image_id int(11) unsigned NOT NULL default '0',
  user_id int(11) unsigned NOT NULL default '0',
  res varchar(14) default NULL,
  remote_addr varchar(32) default NULL,
  ts timestamp(14) NOT NULL,
  PRIMARY KEY  (id)
) TYPE=MyISAM;

