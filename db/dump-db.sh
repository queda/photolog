#! /bin/sh

LST='book book_image image image_note image_view site site_image'

HST=-hlocalhost
HST=-hveuropa

mysqldump -ed photolog > db-photolog.sql

for i in $LST; do
    mysqldump $HST photolog -ed $i > $i.sql
done

mysqldump photolog user > user.sql
