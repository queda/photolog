-- MySQL dump 8.23
--
-- Host: veuropa    Database: photolog
---------------------------------------------------------
-- Server version	3.23.58

--
-- Table structure for table `image_note`
--

CREATE TABLE image_note (
  id int(11) unsigned NOT NULL auto_increment,
  image_id int(11) unsigned NOT NULL default '0',
  user_id int(11) unsigned NOT NULL default '0',
  note varchar(255) default NULL,
  ts timestamp(14) NOT NULL,
  PRIMARY KEY  (id),
  UNIQUE KEY imageuser (image_id,user_id)
) TYPE=MyISAM;

