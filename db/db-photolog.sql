-- MySQL dump 8.23
--
-- Host: localhost    Database: photolog
---------------------------------------------------------
-- Server version	3.23.58

--
-- Table structure for table `book`
--

CREATE TABLE book (
  id int(11) unsigned NOT NULL auto_increment,
  p_id int(11) unsigned NOT NULL default '0',
  user_id int(11) unsigned NOT NULL default '0',
  name varchar(255) default NULL,
  notes text,
  status set('a','c') default 'a',
  priv tinyint(3) default '127',
  datedesc varchar(32) NOT NULL default '',
  image_id int(11) NOT NULL default '0',
  cd date default NULL,
  ts timestamp(14) NOT NULL,
  PRIMARY KEY  (id),
  UNIQUE KEY un (user_id,name),
  FULLTEXT KEY nn (name,notes)
) TYPE=MyISAM;

--
-- Table structure for table `book_image`
--

CREATE TABLE book_image (
  id int(11) unsigned NOT NULL auto_increment,
  book_id int(11) unsigned NOT NULL default '0',
  image_id int(11) unsigned NOT NULL default '0',
  PRIMARY KEY  (id),
  UNIQUE KEY bookimage (book_id,image_id)
) TYPE=MyISAM;

--
-- Table structure for table `image`
--

CREATE TABLE image (
  id int(11) unsigned NOT NULL auto_increment,
  user_id int(11) unsigned NOT NULL default '0',
  name varchar(32) NOT NULL default '',
  dir varchar(255) NOT NULL default '',
  status set('a') NOT NULL default '',
  media set('i','p','v') NOT NULL default '',
  priv tinyint(3) NOT NULL default '127',
  title varchar(128) default NULL,
  notes text,
  res varchar(14) default NULL,
  views int(11) unsigned default '0',
  cd datetime default NULL,
  ts timestamp(14) NOT NULL,
  PRIMARY KEY  (id),
  UNIQUE KEY namedir (name,dir),
  FULLTEXT KEY namedirnotes (name,dir,notes)
) TYPE=MyISAM;

--
-- Table structure for table `image_note`
--

CREATE TABLE image_note (
  id int(11) unsigned NOT NULL auto_increment,
  image_id int(11) unsigned NOT NULL default '0',
  user_id int(11) unsigned NOT NULL default '0',
  note varchar(255) default NULL,
  ts timestamp(14) NOT NULL,
  PRIMARY KEY  (id),
  UNIQUE KEY imageuser (image_id,user_id)
) TYPE=MyISAM;

--
-- Table structure for table `image_view`
--

CREATE TABLE image_view (
  id int(11) unsigned NOT NULL auto_increment,
  image_id int(11) unsigned NOT NULL default '0',
  user_id int(11) unsigned NOT NULL default '0',
  res varchar(14) default NULL,
  remote_addr varchar(32) default NULL,
  ts timestamp(14) NOT NULL,
  PRIMARY KEY  (id)
) TYPE=MyISAM;

--
-- Table structure for table `site`
--

CREATE TABLE site (
  id int(11) unsigned NOT NULL auto_increment,
  server_name varchar(255) default NULL,
  notes text,
  status set('a','c') default 'a',
  priv tinyint(3) default '127',
  cd date default NULL,
  ts timestamp(14) NOT NULL,
  PRIMARY KEY  (id)
) TYPE=MyISAM;

--
-- Table structure for table `site_image`
--

CREATE TABLE site_image (
  id int(11) unsigned NOT NULL auto_increment,
  image_id int(11) unsigned NOT NULL default '0',
  site_id int(11) unsigned NOT NULL default '0',
  size int(11) default NULL,
  res varchar(14) default NULL,
  ts timestamp(14) NOT NULL,
  PRIMARY KEY  (id),
  UNIQUE KEY image_site (image_id,site_id)
) TYPE=MyISAM;

--
-- Table structure for table `user`
--

CREATE TABLE user (
  id int(11) unsigned NOT NULL auto_increment,
  email varchar(50) NOT NULL default '',
  passwd varchar(20) NOT NULL default '',
  name varchar(50) NOT NULL default '',
  address varchar(81) NOT NULL default '',
  city varchar(41) NOT NULL default '',
  state char(3) NOT NULL default '',
  zip varchar(10) NOT NULL default '',
  phone varchar(12) NOT NULL default '',
  country varchar(41) NOT NULL default '',
  visits int(11) default '0',
  priv tinyint(3) default '0',
  img_size tinyint(3) default NULL,
  cd datetime default NULL,
  ts timestamp(14) NOT NULL,
  PRIMARY KEY  (id),
  UNIQUE KEY email (email)
) TYPE=MyISAM;

